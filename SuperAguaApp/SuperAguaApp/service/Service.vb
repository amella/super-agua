﻿Public Class Service

    Public _superAguaDAO As SuperAguaDAO

    Public Function getUsuarios()
        Dim usuarios As List(Of Usuario) = Me._superAguaDAO.getUsuarios()
        Return usuarios
    End Function

    Public Function getUsuarioByUsername(ByRef username As String)
        Return Me._superAguaDAO.getUsuarioByUsername(username)
    End Function

    Public Function existsUsuario(ByRef username As String)
        Return Me._superAguaDAO.existsUsuario(username)
    End Function

    Public Sub insertUsuario(ByRef user As Usuario)
        Me._superAguaDAO.insertUsuario(user)
    End Sub

    Public Function updateUsuario(ByRef user As Usuario)
        Return Me._superAguaDAO.updateUsuario(user)
    End Function

    Public Function deleteUsuario(ByRef user As Usuario)
        Return Me._superAguaDAO.deleteUsuario(user)
    End Function

    Public Function getRutas()

        Dim rutas As List(Of String) = Me._superAguaDAO.getRutas()

        Return rutas
       
    End Function

    Public Function deleteRutaEnCarga(ByRef ruta As String)
        Return Me._superAguaDAO.deleteRutaEnCarga(ruta)
    End Function

    Public Function deleteRuta(ByRef ruta As String)
        Return Me._superAguaDAO.deleteRuta(ruta)
    End Function



    Public Function existRuta(ByRef ruta As String)
        Return _superAguaDAO.existRuta(ruta)
    End Function

    Public Function updateRutaEnCarga(ByRef ruta As String, ByRef newRuta As String)
        Return _superAguaDAO.updateRutaEnCarga(ruta, newRuta)
    End Function

    Public Function updateRuta(ByRef ruta As String, ByRef newRuta As String)
        Return _superAguaDAO.updateRuta(ruta, newRuta)
    End Function

    Public Sub updateRutas(ByRef foo As List(Of String))
        _superAguaDAO.updateRutas(foo)
    End Sub

    Public Function insertRuta(ByRef ruta As String)
        Return _superAguaDAO.insertRuta(ruta)
    End Function

    Public Function existCarga(ByRef id As Long)

        Return _superAguaDAO.existCarga(id)

    End Function

    Public Function existPuerta(ByRef id As Long)

        Return _superAguaDAO.existPuerta(id)

    End Function

    Public Function existRutaViajeNoCerrado(ByRef ruta As String)

        Return _superAguaDAO.existRutaViajeNoCerrado(ruta)

    End Function

    Public Function existRegreso(ByRef id As Long)

        Return _superAguaDAO.existRegreso(id)

    End Function

    Public Function getCarga(ByRef id As Long)

        Return _superAguaDAO.getCarga(id)

    End Function

    Public Function getLastCarga(ByRef ruta As String)

        Return _superAguaDAO.getLastCarga(ruta)

    End Function

    Public Function getPuerta(ByRef id As Long)

        Return _superAguaDAO.getPuerta(id)

    End Function

    Public Function getRegreso(ByRef id As Long)

        Return _superAguaDAO.getRegreso(id)

    End Function

    Public Function insertCarga(ByRef carga As Carga)

        Return _superAguaDAO.cargaInsert(carga)

    End Function

    Public Sub insertPuerta(ByRef puerta As Puerta)

        _superAguaDAO.puertaInsert(puerta)

    End Sub

    Public Sub insertRegreso(ByRef regreso As Regreso)

        _superAguaDAO.regresoInsert(regreso)

    End Sub

    Public Sub updateCarga(ByRef carga As Carga)
        _superAguaDAO.cargaUpdate(carga)
    End Sub

    Public Sub updatePuerta(ByRef puerta As Puerta)
        _superAguaDAO.puertaUpdate(puerta)
    End Sub

    Public Sub updateRegreso(ByRef regreso As Regreso)
        _superAguaDAO.regresoUpdate(regreso)
    End Sub

End Class
