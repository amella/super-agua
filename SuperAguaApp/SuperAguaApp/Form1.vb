﻿Public Class Form1

    Private rutas As List(Of String)

    Private Function getServicio()

        Dim servicio As Service = New Service()
        Dim connectionManager As ConnectionManager = New ConnectionManager()
        Dim superAguaDAO As SuperAguaDAO = New SuperAguaDAO()

        superAguaDAO._connectionManager = connectionManager
        servicio._superAguaDAO = superAguaDAO

        Return servicio

    End Function

    Private Sub btnGuardarCarga_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarCarga.Click

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If

        Dim rutaStr As String = Me.tbRutaCarga.Text.Trim
        Dim cantidadStr As String = Me.tbCantidadCarga.Text
        Dim idStr As String = Me.tbIdCarga.Text

        Dim liquidoStr = Me.tbLiquidoCarga.Text
        Dim destiladoStr = Me.tbDestiladoCarga.Text
        Dim hieloStr = Me.tbHieloCarga.Text
        Dim caja355Str = Me.tbCaja355Carga.Text
        Dim caja500Str = Me.tbCaja500Carga.Text
        Dim caja1000Str = Me.tbCaja1000Carga.Text
        Dim caja1500Str = Me.tbCaja1500Carga.Text
        Dim envaseStr = Me.tbEnvaseCarga.Text

        Dim servicio As Service = Nothing
        Dim carga As Carga = Nothing

        Dim id As Long

        If Not (String.Empty = rutaStr Or _
                String.Empty = liquidoStr Or String.Empty = destiladoStr Or _
                String.Empty = hieloStr Or String.Empty = caja355Str Or _
                String.Empty = caja500Str Or String.Empty = caja1000Str Or _
                String.Empty = caja1500Str Or String.Empty = envaseStr) Then

            Try

                carga = New Carga()

                carga._app = Long.Parse(liquidoStr)
                carga._apla = Long.Parse(destiladoStr)
                carga._hielo = Long.Parse(hieloStr)
                carga._caja355 = Long.Parse(caja355Str)
                carga._caja500 = Long.Parse(caja500Str)
                carga._caja1000 = Long.Parse(caja1000Str)
                carga._caja1500 = Long.Parse(caja1500Str)
                carga._envase = Long.Parse(envaseStr)
                carga._ruta = rutaStr

                servicio = Me.getServicio()

                Try

                    If String.Empty <> idStr Then

                        id = Long.Parse(idStr)

                        Dim exist As Boolean = servicio.existCarga(id)

                        If exist = True Then

                            carga._id = id

                            servicio.updateCarga(carga)

                            carga = servicio.getCarga(id)

                            Me.tbCantidadCarga.Text = carga.getCantidad()
                            Me.tbFechaCarga.Text = carga._fecha
                            Me.tbRutaCarga.Text = carga._ruta

                            Me.tbLiquidoCarga.Text = carga._app
                            Me.tbDestiladoCarga.Text = carga._apla
                            Me.tbHieloCarga.Text = carga._hielo
                            Me.tbCaja355Carga.Text = carga._caja355
                            Me.tbCaja500Carga.Text = carga._caja500
                            Me.tbCaja1000Carga.Text = carga._caja1000
                            Me.tbCaja1500Carga.Text = carga._caja1500
                            Me.tbEnvaseCarga.Text = carga._envase

                            MessageBox.Show("Información actualizada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                            Me.limpiarCarga()

                        Else

                            'Existe un viaje X con la ruta ingresada que este abierto?
                            Dim cnt As Long = servicio.existRutaViajeNoCerrado(carga._ruta)

                            If cnt <> 0 Then

                                MessageBox.Show("Unidad surtida falta dar salida.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Return

                            End If

                            id = servicio.insertCarga(carga)

                            Me.tbIdCarga.Text = id

                            carga = servicio.getCarga(id)

                            Me.tbCantidadCarga.Text = carga.getCantidad()
                            Me.tbFechaCarga.Text = carga._fecha
                            Me.tbRutaCarga.Text = carga._ruta

                            MessageBox.Show("Información almacenada, id de viaje: " & carga._id, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        End If

                    Else

                        'Existe un viaje X con la ruta ingresada que este abierto?
                        Dim fe As Boolean = servicio.existRutaViajeNoCerrado(carga._ruta)

                        If fe Then

                            MessageBox.Show("Unidad surtida falta dar salida.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Return

                        End If

                        id = servicio.insertCarga(carga)

                        Me.tbIdCarga.Text = id

                        carga = servicio.getCarga(id)

                        Me.tbCantidadCarga.Text = carga.getCantidad()
                        Me.tbFechaCarga.Text = carga._fecha
                        Me.tbRutaCarga.Text = carga._ruta

                        Me.tbLiquidoCarga.Text = carga._app
                        Me.tbDestiladoCarga.Text = carga._apla
                        Me.tbHieloCarga.Text = carga._hielo
                        Me.tbCaja355Carga.Text = carga._caja355
                        Me.tbCaja500Carga.Text = carga._caja500
                        Me.tbCaja1000Carga.Text = carga._caja1000
                        Me.tbCaja1500Carga.Text = carga._caja1500
                        Me.tbEnvaseCarga.Text = carga._envase

                        MessageBox.Show("Información almacenada, id de viaje: " & carga._id, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Me.limpiarCarga()

                    End If

                Catch ex As Exception
                    MessageBox.Show("Asegurese de haber ingresado un valor numérico para el id de carga.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try

            Catch ex As Exception

                MessageBox.Show("Asegurese de haber ingresado un valor numérico para la cantidad.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Return

            End Try
        Else

            MessageBox.Show("Asegurese de haber ingresado los campos requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End If


    End Sub

    Private Sub btnBuscarCarga_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarCarga.Click

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If


        Dim id As Long

        Try

            id = Long.Parse(Me.tbIdCarga.Text)

        Catch ex As Exception

            MessageBox.Show("Asegurese de haber ingresado un valor numérico para el id.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Return

        End Try

        Dim servicio As Service = Me.getServicio()
        Dim existe As Boolean = servicio.existCarga(id)

        If existe = False Then

            MessageBox.Show("No se encontró información para ese id.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Else

            Dim carga As Carga = servicio.getCarga(id)

            Me.tbCantidadCarga.Text = carga.getCantidad()
            Me.tbFechaCarga.Text = carga._fecha
            Me.tbRutaCarga.Text = carga._ruta

            Me.tbLiquidoCarga.Text = carga._app
            Me.tbDestiladoCarga.Text = carga._apla
            Me.tbHieloCarga.Text = carga._hielo
            Me.tbCaja355Carga.Text = carga._caja355
            Me.tbCaja500Carga.Text = carga._caja500
            Me.tbCaja1000Carga.Text = carga._caja1000
            Me.tbCaja1500Carga.Text = carga._caja1500
            Me.tbEnvaseCarga.Text = carga._envase

        End If

    End Sub


    Private Sub btnGuardarPuerta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarPuerta.Click

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If

        Dim liquidoStr As String = Me.tbLiquidoPuerta.Text
        Dim destiladoStr As String = Me.tbDestiladoPuerta.Text
        Dim hieloStr As String = Me.tbHieloPuerta.Text
        Dim caja355Str As String = Me.tbCaja355Puerta.Text
        Dim caja500Str As String = Me.tbCaja500Puerta.Text
        Dim caja1000Str As String = Me.tbCaja1000Puerta.Text
        Dim caja1500Str As String = Me.tbCaja1500Puerta.Text
        Dim llavesStr As String = Me.tbLlavesPuerta.Text
        Dim envaseStr As String = Me.tbEnvasePuerta.Text
        Dim idStr As String = Me.tbIdPuerta.Text
        Dim obs As String = String.Empty
        Dim servicio As Service = Nothing

        Dim id As Long
        Dim liquido As Long
        Dim destilado As Long
        Dim hielo As Long
        Dim caja355 As Long
        Dim caja500 As Long
        Dim caja1000 As Long
        Dim caja1500 As Long
        Dim llaves As Long
        Dim envase As Long

        If Not (String.Empty = envaseStr Or String.Empty = liquidoStr Or String.Empty = destiladoStr Or String.Empty = hieloStr Or String.Empty = caja355Str Or String.Empty = caja500Str Or String.Empty = caja1500Str Or String.Empty = llavesStr Or String.Empty = idStr) Then

            Try

                id = Long.Parse(idStr)
                liquido = Long.Parse(liquidoStr)
                destilado = Long.Parse(destiladoStr)
                hielo = Long.Parse(hieloStr)
                caja355 = Long.Parse(caja355Str)
                caja500 = Long.Parse(caja500Str)
                caja1000 = Long.Parse(caja1000Str)
                caja1500 = Long.Parse(caja1500Str)
                llaves = Long.Parse(llavesStr)
                envase = Long.Parse(envaseStr)

                Dim puerta As Puerta = New Puerta()
                puerta._hielo = hielo
                puerta._caja355 = caja355
                puerta._caja500 = caja500
                puerta._caja1000 = caja1000
                puerta._caja1500 = caja1500
                puerta._llaves = llaves
                puerta._app = liquido
                puerta._apla = destilado
                puerta._envase = envase
                puerta._id = id


                If String.Empty <> Me.tbObsPuerta.Text Then

                    For Each line In Me.tbObsPuerta.Lines

                        obs = obs & line & " "

                    Next

                End If

                puerta._obs = obs


                servicio = Me.getServicio()

                Dim existe As Boolean = servicio.existCarga(id)

                If existe = False Then

                    MessageBox.Show("No se encontró información para la carga.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Else

                    Dim exist As Boolean = servicio.existPuerta(id)

                    If exist = True Then

                        servicio.updatePuerta(puerta)

                        puerta = servicio.getPuerta(id)

                        Me.tbLiquidoPuerta.Text = puerta._app
                        Me.tbDestiladoPuerta.Text = puerta._apla
                        Me.tbHieloPuerta.Text = puerta._hielo
                        Me.tbCaja355Puerta.Text = puerta._caja355
                        Me.tbCaja500Puerta.Text = puerta._caja500
                        Me.tbCaja1000Puerta.Text = puerta._caja1000
                        Me.tbCaja1500Puerta.Text = puerta._caja1500
                        Me.tbLlavesPuerta.Text = puerta._llaves
                        Me.tbObsPuerta.Text = puerta._obs
                        Me.tbEnvasePuerta.Text = puerta._envase

                        If id <> -1 Then
                            'ir por la ruta
                            Dim carga As Carga = servicio.getCarga(id)

                            Me.tbRutaPuerta.Text = carga._ruta
                            Me.tbIdPuerta.Text = carga._id

                        End If

                        MessageBox.Show("Información actualizada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Me.limpiarPuerta()

                    Else

                        servicio.insertPuerta(puerta)

                        puerta = servicio.getPuerta(id)

                        Me.tbLiquidoPuerta.Text = puerta._app
                        Me.tbDestiladoPuerta.Text = puerta._apla
                        Me.tbHieloPuerta.Text = puerta._hielo
                        Me.tbCaja355Puerta.Text = puerta._caja355
                        Me.tbCaja500Puerta.Text = puerta._caja500
                        Me.tbCaja1000Puerta.Text = puerta._caja1000
                        Me.tbCaja1500Puerta.Text = puerta._caja1500
                        Me.tbLlavesPuerta.Text = puerta._llaves
                        Me.tbObsPuerta.Text = puerta._obs

                        Me.tbEnvasePuerta.Text = puerta._envase

                        If id <> -1 Then
                            'ir por la ruta
                            Dim carga As Carga = servicio.getCarga(id)

                            Me.tbRutaPuerta.Text = carga._ruta
                            Me.tbIdPuerta.Text = carga._id

                        End If

                        MessageBox.Show("Información almacenada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Me.limpiarPuerta()

                    End If

                End If

            Catch ex As Exception

                MessageBox.Show("Asegurese de haber ingresado valores numéricos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Return

            End Try
        Else

            MessageBox.Show("Asegurese de haber ingresado los campos requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End If

    End Sub

    Private Sub btnBuscarPuerta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarPuerta.Click

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If


        'buscar por id de viaje o por ruta, si tenemos el id viaje, mejor por ese, dado que es number y pk

        Dim strId As String = Me.tbIdPuerta.Text
        Dim id As Long = -1
        Dim ruta As String = Me.tbRutaPuerta.Text

        If strId.Trim() = String.Empty And ruta.Trim() = String.Empty Then

            MessageBox.Show("Ingrese el id de viaje ó la ruta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Return

        End If

        strId = strId.Trim()
        ruta = ruta.Trim()

        Dim servicio As Service = Me.getServicio()

        'si traemos el id de viaje, sobre ese nos vamos

        If strId <> String.Empty Then

            Try

                id = Long.Parse(strId)

                Dim carga As Carga = servicio.getCarga(id)

                Me.tbRutaPuerta.Text = carga._ruta

            Catch ex As Exception

                MessageBox.Show("Asegurese de haber ingresado un valor numérico para el id.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Return

            End Try

        Else

            id = servicio.getLastCarga(ruta)

            If id <> -1 Then

                Me.tbIdPuerta.Text = id

            End If

        End If

        Dim existe As Boolean = servicio.existPuerta(id)

        If existe = False Then

            MessageBox.Show("No se encontró información para ese id.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Else

            Dim puerta As Puerta = servicio.getPuerta(id)

            Me.tbLiquidoPuerta.Text = puerta._app
            Me.tbDestiladoPuerta.Text = puerta._apla
            Me.tbHieloPuerta.Text = puerta._hielo
            Me.tbCaja355Puerta.Text = puerta._caja355
            Me.tbCaja500Puerta.Text = puerta._caja500
            Me.tbCaja1000Puerta.Text = puerta._caja1000
            Me.tbCaja1500Puerta.Text = puerta._caja1500
            Me.tbLlavesPuerta.Text = puerta._llaves
            Me.tbObsPuerta.Text = puerta._obs
            Me.tbEnvasePuerta.Text = puerta._envase

            If id <> -1 Then
                'ir por la ruta
                Dim carga As Carga = servicio.getCarga(id)

                Me.tbRutaPuerta.Text = carga._ruta
                Me.tbIdPuerta.Text = carga._id

            End If

        End If

    End Sub

    Private Sub btnGuardarRegreso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarRegreso.Click

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If

        Dim competenciaStr As String = Me.tbCompetenciaRegreso.Text
        Dim llenosStr As String = Me.tbLlenosRegreso.Text
        Dim obsStr As String = String.Empty
        Dim rotosStr As String = Me.tbRotosRegreso.Text
        Dim vaciosStr As String = Me.tbEnvaseRegreso.Text
        Dim prestamoStr As String = Me.tbPrestamoRegreso.Text
        Dim idStr As String = Me.tbIdRegreso.Text

        Dim caja355Str As String = Me.tbCaja355Regreso.Text
        Dim caja500Str As String = Me.tbCaja500Regreso.Text
        Dim caja1000Str As String = Me.tbCaja1000Regreso.Text
        Dim caja1500Str As String = Me.tbCaja1500Regreso.Text
        Dim llavesStr As String = Me.tbLlavesRegreso.Text

        Dim recuperadoStr As String = Me.tbRecuperadosRegreso.Text
        Dim hieloStr As String = Me.tbHieloRegreso.Text

        Dim servicio As Service = Nothing

        Dim competencia As Long
        Dim llenos As Long
        Dim obs As String
        Dim rotos As Long
        Dim vacios As Long
        Dim id As Long

        Dim caja355 As Long
        Dim caja500 As Long
        Dim caja1000 As Long
        Dim caja1500 As Long
        Dim llaves As Long

        Dim prestamo As Long

        Dim recuperado As Long
        Dim hielo As Long

        If Not (String.Empty = prestamoStr Or String.Empty = recuperadoStr Or String.Empty = hieloStr Or String.Empty = caja355Str Or String.Empty = caja500Str Or String.Empty = caja1500Str Or String.Empty = llavesStr Or String.Empty = competenciaStr Or String.Empty = llenosStr Or String.Empty = rotosStr Or String.Empty = vaciosStr Or String.Empty = idStr) Then

            Try

                id = Long.Parse(idStr)
                competencia = Long.Parse(competenciaStr)
                llenos = Long.Parse(llenosStr)
                rotos = Long.Parse(rotosStr)
                vacios = Long.Parse(vaciosStr)
                prestamo = Long.Parse(prestamoStr)
                caja355 = Long.Parse(caja355Str)
                caja500 = Long.Parse(caja500Str)
                caja1000 = Long.Parse(caja1000Str)
                caja1500 = Long.Parse(caja1500Str)
                llaves = Long.Parse(llavesStr)

                hielo = Long.Parse(hieloStr)
                recuperado = Long.Parse(recuperadoStr)

                Dim regreso As Regreso = New Regreso()
                regreso._id = id
                regreso._competencia = competencia
                regreso._lleno = llenos
                regreso._envase = vacios
                regreso._roto = rotos
                regreso._caja355 = caja355
                regreso._caja500 = caja500
                regreso._caja1000 = caja1000
                regreso._caja1500 = caja1500
                regreso._llaves = llaves
                regreso._prestamo = prestamo
                regreso._recuperado = recuperado
                regreso._hielo = hielo

                obs = String.Empty

                If String.Empty <> Me.tbObsRegreso.Text Then

                    For Each line In Me.tbObsRegreso.Lines

                        obs = obs & line & " "

                    Next

                End If

                regreso._obs = obs

                servicio = Me.getServicio()

                Dim existe As Boolean = servicio.existCarga(id)

                If existe = False Then

                    MessageBox.Show("No se encontró información para la carga.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Else


                    Dim carga As Carga = servicio.getCarga(id)

                    Dim k = regreso.getCantidad()
                    Dim l = carga.getCantidad()

                    If Not (k >= l) Then

                        MessageBox.Show("No se puede guardar la información porque la venta en el Regreso es menor a la venta en la Carga.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Return

                    End If

                    Dim exist As Boolean = servicio.existRegreso(id)

                    If exist = True Then

                        servicio.updateRegreso(regreso)

                        regreso = servicio.getRegreso(id)

                        Me.tbCompetenciaRegreso.Text = regreso._competencia
                        Me.tbLlenosRegreso.Text = regreso._lleno
                        Me.tbObsRegreso.Text = regreso._obs
                        Me.tbRotosRegreso.Text = regreso._roto
                        Me.tbEnvaseRegreso.Text = regreso._envase

                        Me.tbCaja355Regreso.Text = regreso._caja355
                        Me.tbCaja500Regreso.Text = regreso._caja500
                        Me.tbCaja1000Regreso.Text = regreso._caja1000
                        Me.tbCaja1500Regreso.Text = regreso._caja1500
                        Me.tbLlavesRegreso.Text = regreso._llaves
                        Me.tbVentaRegreso.Text = regreso.getCantidad()
                        Me.tbPrestamoRegreso.Text = regreso._prestamo

                        Me.tbRecuperadosRegreso.Text = regreso._recuperado
                        Me.tbHieloRegreso.Text = regreso._hielo

                        MessageBox.Show("Información actualizada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Me.limpiarRegreso()

                    Else

                        servicio.insertRegreso(regreso)

                        regreso = servicio.getRegreso(id)

                        Me.tbCompetenciaRegreso.Text = regreso._competencia
                        Me.tbLlenosRegreso.Text = regreso._lleno
                        Me.tbObsRegreso.Text = regreso._obs
                        Me.tbRotosRegreso.Text = regreso._roto
                        Me.tbEnvaseRegreso.Text = regreso._envase

                        Me.tbCaja355Regreso.Text = regreso._caja355
                        Me.tbCaja500Regreso.Text = regreso._caja500
                        Me.tbCaja1000Regreso.Text = regreso._caja1000
                        Me.tbCaja1500Regreso.Text = regreso._caja1500
                        Me.tbLlavesRegreso.Text = regreso._llaves
                        Me.tbVentaRegreso.Text = regreso.getCantidad()
                        Me.tbPrestamoRegreso.Text = regreso._prestamo

                        Me.tbRecuperadosRegreso.Text = regreso._recuperado
                        Me.tbHieloRegreso.Text = regreso._hielo

                        MessageBox.Show("Información almacenada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Me.limpiarRegreso()

                    End If

                    carga._cerrada = 1
                    servicio.updateCarga(carga)

                End If

            Catch ex As Exception

            MessageBox.Show("Asegurese de haber ingresado valores numéricos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Return

        End Try
        Else

            MessageBox.Show("Asegurese de haber ingresado los campos requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End If

    End Sub

    Private Sub btnBuscarRegreso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarRegreso.Click

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If

        Dim id As Long

        Try

            id = Long.Parse(Me.tbIdRegreso.Text)

        Catch ex As Exception

            MessageBox.Show("Asegurese de haber ingresado un valor numérico para el id.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Return

        End Try

        Dim servicio As Service = Me.getServicio()
        Dim existe As Boolean = servicio.existRegreso(id)

        If existe = False Then

            MessageBox.Show("No se encontró información para ese id.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Else

            Dim regreso As Regreso = servicio.getRegreso(id)

            Me.tbCompetenciaRegreso.Text = regreso._competencia
            Me.tbLlenosRegreso.Text = regreso._lleno
            Me.tbObsRegreso.Text = regreso._obs
            Me.tbRotosRegreso.Text = regreso._roto
            Me.tbEnvaseRegreso.Text = regreso._envase
            Me.tbCaja355Regreso.Text = regreso._caja355
            Me.tbCaja500Regreso.Text = regreso._caja500
            Me.tbCaja1000Regreso.Text = regreso._caja1000
            Me.tbCaja1500Regreso.Text = regreso._caja1500
            Me.tbLlavesRegreso.Text = regreso._llaves
            Me.tbVentaRegreso.Text = regreso.getCantidad()
            Me.tbPrestamoRegreso.Text = regreso._prestamo
            Me.tbRecuperadosRegreso.Text = regreso._recuperado
            Me.tbHieloRegreso.Text = regreso._hielo

        End If

    End Sub

    Private Sub hidePanels()

        puertaPanel.Visible = False
        regresoPanel.Visible = False
        cajaPanel.Visible = False
        cargaPanel.Visible = False
        rutaPanel.Visible = False
        loginPanel.Visible = False
        usuarioPanel.Visible = False

    End Sub

    Private Sub CargaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargaToolStripMenuItem.Click

        Me.hidePanels()
        Me.limpiarCarga()

        cargaPanel.Visible = True

        Me.tbIdCarga.Focus()

    End Sub

    Private Sub PuertaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PuertaToolStripMenuItem.Click

        Me.hidePanels()
        Me.limpiarPuerta()
        puertaPanel.Visible = True


    End Sub

    Private Sub RegresoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresoToolStripMenuItem.Click

        Me.hidePanels()
        Me.limpiarRegreso()
        regresoPanel.Visible = True

    End Sub

    Private Sub CajaToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CajaToolStripMenuItem.Click

        puertaPanel.Visible = False
        regresoPanel.Visible = False
        cajaPanel.Visible = False
        cargaPanel.Visible = False
        Me.limpiarCaja()
        cajaPanel.Visible = True

    End Sub

    Private Sub limpiarRegreso()

        Me.tbIdRegreso.Text = String.Empty
        Me.tbRotosRegreso.Text = String.Empty
        Me.tbCompetenciaRegreso.Text = String.Empty
        Me.tbLlenosRegreso.Text = String.Empty
        Me.tbObsRegreso.Text = String.Empty
        Me.tbEnvaseRegreso.Text = String.Empty
        Me.tbCaja355Regreso.Text = String.Empty
        Me.tbCaja500Regreso.Text = String.Empty
        Me.tbCaja1000Regreso.Text = String.Empty
        Me.tbCaja1500Regreso.Text = String.Empty
        Me.tbLlavesRegreso.Text = String.Empty
        Me.tbVentaRegreso.Text = 0
        Me.tbPrestamoRegreso.Text = String.Empty
        Me.tbRecuperadosRegreso.Text = String.Empty
        Me.tbHieloRegreso.Text = String.Empty

        Me.tbIdRegreso.Focus()

    End Sub

    Private Sub btnLimpiarRegreso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiarRegreso.Click
        Me.limpiarRegreso()      
    End Sub

    Private Sub limpiarPuerta()

        Me.tbLiquidoPuerta.Text = String.Empty
        Me.tbRutaPuerta.SelectedIndex = -1
        Me.tbDestiladoPuerta.Text = String.Empty
        Me.tbHieloPuerta.Text = String.Empty
        Me.tbCaja355Puerta.Text = String.Empty
        Me.tbCaja500Puerta.Text = String.Empty
        Me.tbCaja1500Puerta.Text = String.Empty
        Me.tbCaja1000Puerta.Text = String.Empty
        Me.tbLlavesPuerta.Text = String.Empty
        Me.tbIdPuerta.Text = String.Empty
        Me.tbObsPuerta.Text = String.Empty
        Me.tbEnvasePuerta.Text = String.Empty

        Me.tbRutaPuerta.Focus()

    End Sub

    Private Sub btnLimpiarPuerta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiarPuerta.Click

        Me.limpiarPuerta()

    End Sub

    Private Sub limpiarCarga()

        Me.tbIdCarga.Text = String.Empty
        Me.tbFechaCarga.Text = String.Empty
        Me.tbCantidadCarga.Text = 0
        Me.tbRutaCarga.SelectedIndex = -1
        Me.tbLiquidoCarga.Text = String.Empty
        Me.tbDestiladoCarga.Text = String.Empty
        Me.tbHieloCarga.Text = String.Empty
        Me.tbCaja355Carga.Text = String.Empty
        Me.tbCaja500Carga.Text = String.Empty
        Me.tbCaja1500Carga.Text = String.Empty
        Me.tbCaja1000Carga.Text = String.Empty
        Me.tbEnvaseCarga.Text = String.Empty

        Me.tbIdCarga.Focus()

    End Sub

    Private Sub btnLimpiarCarga_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiarCarga.Click

        Me.limpiarCarga()

    End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcercaDeToolStripMenuItem.Click
        AboutBox1.Show()
    End Sub

    Private Sub ConfiguraciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguraciónToolStripMenuItem.Click
        Preferences.Show()
    End Sub

    Private Sub Panel3_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles cajaPanel.Paint

    End Sub

    Private Sub limpiarCaja()

        Me.tbIdCaja.Text = String.Empty
        Me.tbLiquidoCargaCaja.Text = String.Empty
        Me.tbDestiladoCargaCaja.Text = String.Empty
        Me.tbHieloCargaCaja.Text = String.Empty
        Me.tb355CargaCaja.Text = String.Empty
        Me.tb500CargaCaja.Text = String.Empty
        Me.tb1000CargaCaja.Text = String.Empty
        Me.tb1500CargaCaja.Text = String.Empty
        Me.tbEnvaseCargaCaja.Text = String.Empty
        Me.tbRotosRegresoCaja.Text = String.Empty
        Me.tbCompetenciaRegresoCaja.Text = String.Empty
        Me.tbEnvaseRegresoCaja.Text = String.Empty
        Me.tbLlenosRegresoCaja.Text = String.Empty
        Me.tbLlavesRegresoCaja.Text = String.Empty
        Me.tbVentRegresoCaja.Text = String.Empty
        Me.tb355RegresoCaja.Text = String.Empty
        Me.tb500RegresoCaja.Text = String.Empty
        Me.tb1000RegresoCaja.Text = String.Empty
        Me.tb1500RegresoCaja.Text = String.Empty
        Me.tbObsRegresoCaja.Text = String.Empty
        Me.tbPrestamoRegresoCaja.Text = String.Empty
        Me.tbHieloRegresoCaja.Text = String.Empty
        Me.tbRecuperadosRegresoCaja.Text = String.Empty
        Me.cbCerradaCaja.Checked = False
        Me.lTotalCaja.Text = String.Empty
        Me.iTotalHielo.Text = String.Empty

        Me.tbLiquidoPuertaCaja.Text = String.Empty
        Me.tbDestiladoPuertaCaja.Text = String.Empty
        Me.tbHieloPuertaCaja.Text = String.Empty
        Me.tbLlavesPuertaCaja.Text = String.Empty
        Me.tb355PuertaCaja.Text = String.Empty
        Me.tb500PuertaCaja.Text = String.Empty
        Me.tb1000PuertaCaja.Text = String.Empty
        Me.tb1500PuertaCaja.Text = String.Empty
        Me.tbEnvasePuertaCaja.Text = String.Empty
        Me.tbDif355.Text = String.Empty
        Me.tbDif500.Text = String.Empty
        Me.tbDif1000.Text = String.Empty
        Me.tbDif1500.Text = String.Empty
        Me.tbObsPuertaCaja.Text = String.Empty

        Me.tbIdCaja.Focus()

    End Sub
    Private Sub btnLimpiarCaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiarCaja.Click

        Me.limpiarCaja()

    End Sub

    Private Sub btnBuscarCaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarCaja.Click

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If

        Dim idStr As String = Me.tbIdCaja.Text

        Dim d355 As Long = 0
        Dim d500 As Long = 0
        Dim d1000 As Long = 0
        Dim d1500 As Long = 0
        Dim dHielo As Long = 0

        Dim id As Long

        Try

            id = Long.Parse(idStr)

        Catch ex As Exception

            MessageBox.Show("Asegurese de haber ingresado un valor numérico para el id.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Return

        End Try

        Dim servicio As Service = Me.getServicio()

        Dim existe As Boolean = servicio.existCarga(id)

        If existe = False Then

            MessageBox.Show("No se encontró información para ese id.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Else

            Dim carga As Carga = servicio.getCarga(id)

            Me.tbLiquidoCargaCaja.Text = carga._app
            Me.tbDestiladoCargaCaja.Text = carga._apla
            Me.tbHieloCargaCaja.Text = carga._hielo
            Me.tb355CargaCaja.Text = carga._caja355
            Me.tb500CargaCaja.Text = carga._caja500
            Me.tb1000CargaCaja.Text = carga._caja1000
            Me.tb1500CargaCaja.Text = carga._caja1500
            Me.tbEnvaseCargaCaja.Text = carga._envase

            Me.cbCerradaCaja.Checked = False

            Me.lTotalCaja.Text = String.Empty

            If carga._cerrada = 0 Then
                Me.cbCerradaCaja.Checked = False
            Else
                Me.cbCerradaCaja.Checked = True
            End If

            existe = False

            'Obtener la Puerta
            Dim puerta As Puerta = Nothing
            existe = servicio.existPuerta(id)
            If existe = True Then
                puerta = servicio.getPuerta(id)

                Me.tbLiquidoPuertaCaja.Text = puerta._app
                Me.tbDestiladoPuertaCaja.Text = puerta._apla
                Me.tbHieloPuertaCaja.Text = puerta._hielo
                Me.tbLlavesPuertaCaja.Text = puerta._llaves
                Me.tb355PuertaCaja.Text = puerta._caja355
                Me.tb500PuertaCaja.Text = puerta._caja500
                Me.tb1000PuertaCaja.Text = puerta._caja1000
                Me.tb1500PuertaCaja.Text = puerta._caja1500
                Me.tbObsPuertaCaja.Text = puerta._obs
                Me.tbEnvasePuertaCaja.Text = puerta._envase
                d355 = puerta._caja355
                d500 = puerta._caja500
                d1000 = puerta._caja1000
                d1500 = puerta._caja1500
                dHielo = puerta._hielo

            End If
            'Obtener el Regreso
            Dim regreso As Regreso = Nothing
            existe = servicio.existRegreso(id)
            If existe = True Then

                regreso = servicio.getRegreso(id)
                Me.tbRotosRegresoCaja.Text = regreso._roto
                Me.tbCompetenciaRegresoCaja.Text = regreso._competencia
                Me.tbEnvaseRegresoCaja.Text = regreso._envase
                Me.tbLlenosRegresoCaja.Text = regreso._lleno
                Me.tbLlavesRegresoCaja.Text = regreso._llaves
                Me.tbVentRegresoCaja.Text = regreso.getCantidad()
                Me.tb355RegresoCaja.Text = regreso._caja355
                Me.tb500RegresoCaja.Text = regreso._caja500
                Me.tb1000RegresoCaja.Text = regreso._caja1000
                Me.tb1500RegresoCaja.Text = regreso._caja1500
                Me.tbObsRegresoCaja.Text = regreso._obs
                Me.tbPrestamoRegresoCaja.Text = regreso._prestamo
                Me.tbHieloRegresoCaja.Text = regreso._hielo
                Me.tbRecuperadosRegresoCaja.Text = regreso._recuperado

                Dim suma As Long = 0

                suma = suma + Long.Parse(regreso._roto)
                suma = suma + Long.Parse(regreso._envase)
                suma = suma + Long.Parse(regreso._competencia)
                suma = suma + Long.Parse(regreso._recuperado)
                suma = suma + Long.Parse(regreso._prestamo)

                d355 = d355 - regreso._caja355
                d500 = d500 - regreso._caja500
                d1000 = d1000 - regreso._caja1000
                d1500 = d1500 - regreso._caja1500
                dHielo = dHielo - regreso._hielo

                Me.lTotalCaja.Text = suma & " Garrafones"
                Me.iTotalHielo.Text = dHielo & " Hielos"

            End If

            Me.tbDif355.Text = d355
            Me.tbDif500.Text = d500
            Me.tbDif1000.Text = d1000
            Me.tbDif1500.Text = d1500

        End If


    End Sub

    Private Sub btnGuardarCaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarCaja.Click

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If

        Dim idStr As String = Me.tbIdCaja.Text

        Dim id As Long

        Try

            id = Long.Parse(idStr)

        Catch ex As Exception

            MessageBox.Show("Asegurese de haber ingresado un valor numérico para el id.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Return

        End Try

        Dim servicio As Service = Me.getServicio()

        Dim existe As Boolean = servicio.existCarga(id)

        If existe = False Then

            MessageBox.Show("No se encontró información para ese id.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Else

            Dim carga As Carga = servicio.getCarga(id)

            If Me.cbCerradaCaja.Checked = False Then
                carga._cerrada = 0
            Else
                carga._cerrada = 1
            End If

            servicio.updateCarga(carga)

            MessageBox.Show("Información actualizada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Me.limpiarCaja()

        End If


    End Sub

    Private Sub actualizaCampoCantidadCarga(ByRef tb As System.Windows.Forms.TextBox)

        Dim foo As Long = 0
        Dim fooStr As String = tb.Text

        Try

            foo = Long.Parse(fooStr)
            Dim t As Long = Long.Parse(Me.tbCantidadCarga.Text)
            t = t + foo
            Me.tbCantidadCarga.Text = t

        Catch ex As Exception

        End Try

    End Sub

    Private Sub actualizaCampoVentaRegreso(ByRef tb As System.Windows.Forms.TextBox)

        Dim foo As Long = 0
        Dim fooStr As String = tb.Text

        Try

            foo = Long.Parse(fooStr)
            Dim t As Long = Long.Parse(Me.tbVentaRegreso.Text)
            t = t + foo
            Me.tbVentaRegreso.Text = t

        Catch ex As Exception

        End Try

    End Sub

    Private Sub tbLiquidoCarga_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbLiquidoCarga.Leave
        doUpdateCargaField()
    End Sub

    Private Sub tbDestiladoCarga_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbDestiladoCarga.Leave
        doUpdateCargaField()
    End Sub

    Private Sub tbRotosRegreso_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbRotosRegreso.Leave
        doUpdateRegresoField()
    End Sub

    Private Sub tbEnvaseRegreso_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbEnvaseRegreso.Leave
        doUpdateRegresoField()
    End Sub

    Private Sub tbCompetenciaRegreso_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbCompetenciaRegreso.Leave
        doUpdateRegresoField()
    End Sub

    Private Sub tbPrestamoRegreso_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbPrestamoRegreso.Leave
        doUpdateRegresoField()
    End Sub

    Private Sub doUpdateRegresoField()

        Me.tbVentaRegreso.Text = 0

        actualizaCampoVentaRegreso(Me.tbRotosRegreso)
        actualizaCampoVentaRegreso(Me.tbEnvaseRegreso)
        actualizaCampoVentaRegreso(Me.tbPrestamoRegreso)
        actualizaCampoVentaRegreso(Me.tbCompetenciaRegreso)

    End Sub

    Private Sub doUpdateCargaField()
        Me.tbCantidadCarga.Text = 0
        actualizaCampoCantidadCarga(tbDestiladoCarga)
        actualizaCampoCantidadCarga(tbLiquidoCarga)
    End Sub

    Private Sub load_rutas()

        Me.tbRutaCarga.Items.Clear()
        Me.cbRutas.Items.Clear()
        Me.tbRutaPuerta.Items.Clear()
        Me.lbRuta.Items.Clear()


        Dim servicio As Service = Me.getServicio()
        Dim rutas As List(Of String) = servicio.getRutas()

        Me.tbRutaCarga.Items.AddRange(rutas.ToArray())
        Me.cbRutas.Items.AddRange(rutas.ToArray())
        Me.tbRutaPuerta.Items.AddRange(rutas.ToArray())
        Me.lbRuta.Items.AddRange(rutas.ToArray())

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If

        Me.load_rutas()


        Me.CatalogosToolStripMenuItem.Visible = False
        Me.ModulosToolStripMenuItem.Visible = False

    End Sub

    Private Sub cbRutas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbRutas.SelectedIndexChanged
        Me.tbRutaCatalogo.Text = Me.cbRutas.Text
    End Sub

    Private Sub RutasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RutasToolStripMenuItem.Click
        Me.load_rutas()
        Me.hidePanels()
        Me.tbRutaCatalogo.Text = String.Empty
        Me.rutaPanel.Visible = True

        Me.cbRutas.Focus()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim rutaTxt As String = Me.tbRutaCatalogo.Text.Trim
        Dim ruta = Me.cbRutas.Text.Trim.ToUpper

        If rutaTxt <> String.Empty Then

            rutaTxt = rutaTxt.ToUpper()

            Dim servicio As Service = Me.getServicio()
            Dim existe As Boolean = servicio.existRuta(rutaTxt)

            'verificar que el nuevo valor no esta en la base
            If Not existe Then

                'actualizar todos los viajes con esa ruta
                servicio.updateRutaEnCarga(ruta, rutaTxt)
                'actualizar el id de la ruta
                servicio.updateRuta(ruta, rutaTxt)

                Me.load_rutas()

                Me.tbRutaCatalogo.Text = String.Empty

                MessageBox.Show("Ruta actualizada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
             
            Else

                MessageBox.Show("La ruta ya se encuentra en el catálogo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        End If
        
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim rutaTxt As String = Me.tbRutaCatalogo.Text.Trim

        If rutaTxt <> String.Empty Then

            rutaTxt = rutaTxt.ToUpper()

            Dim servicio As Service = Me.getServicio()
            Dim existe As Boolean = servicio.existRuta(rutaTxt)

            'verificar que el nuevo valor no esta en la base
            If Not existe Then
           
                servicio.insertRuta(rutaTxt)

                Me.load_rutas()

                Me.tbRutaCatalogo.Text = String.Empty

                MessageBox.Show("Ruta almacenada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Else

                MessageBox.Show("La ruta ya está en el catálogo, no se almacenó la información.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        If Me.cbRutas.SelectedIndex = -1 Then

            Return

        End If

        Dim rutaTxt = Me.cbRutas.Text.Trim.ToUpper()

        rutaTxt = rutaTxt.ToUpper()

        Dim servicio As Service = Me.getServicio()

        'eliminar todos los viajes con esa ruta
        servicio.deleteRutaEnCarga(rutaTxt)
        'actualizar el id de la ruta
        servicio.deleteRuta(rutaTxt)

        Me.load_rutas()

        Me.tbRutaCatalogo.Text = String.Empty

        MessageBox.Show("Ruta eliminada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub tbIdRegreso_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbIdRegreso.Leave

        Dim idStr As String = Me.tbIdRegreso.Text.Trim()

        Try

            Dim id = Long.Parse(idStr)
            Dim servicio As Service = Me.getServicio()

            Dim existeRegreso As Boolean = servicio.existRegreso(id)

            If Not existeRegreso Then

                Dim existePuerta As Boolean = servicio.existPuerta(id)

                If existePuerta Then

                    Dim puerta As Puerta = servicio.getPuerta(id)
                    Me.tbObsRegreso.AppendText(puerta._obs & Environment.NewLine)

                End If

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        Dim curItem As Integer = Me.lbRuta.SelectedIndex

        If -1 = curItem Then
            Return
        End If

        Dim count = Me.lbRuta.Items.Count
        Dim foo As String = Me.lbRuta.SelectedItem.ToString()

        If 0 = curItem Then           

            Me.lbRuta.Items.RemoveAt(0)
            Me.lbRuta.Items.Add(foo)
            Me.lbRuta.SelectedIndex = count - 1

        Else

            Dim upIndex As Integer = curItem - 1
            Dim el As String = Me.lbRuta.Items.Item(upIndex).ToString()

            Me.lbRuta.Items.Item(curItem) = el

            Me.lbRuta.Items.Item(upIndex) = foo
            Me.lbRuta.SelectedIndex = upIndex

        End If

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        Dim curItem As Integer = Me.lbRuta.SelectedIndex

        If -1 = curItem Then
            Return
        End If

        Dim count = Me.lbRuta.Items.Count
        Dim foo As String = Me.lbRuta.SelectedItem.ToString()

        If (count - 1) = curItem Then

            Me.lbRuta.Items.RemoveAt(count - 1)
            Me.lbRuta.Items.Insert(0, foo)         
            Me.lbRuta.SelectedIndex = 0

        Else

            Dim downIndex As Integer = curItem + 1
            Dim el As String = Me.lbRuta.Items.Item(downIndex).ToString()

            Me.lbRuta.Items.Item(curItem) = el

            Me.lbRuta.Items.Item(downIndex) = foo
            Me.lbRuta.SelectedIndex = downIndex

        End If

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click

        Dim rutas As List(Of String) = New List(Of String)
        Dim count = Me.lbRuta.Items.Count

        For cnt As Integer = 0 To count - 1
            rutas.Add(Me.lbRuta.Items.Item(cnt).ToString)
        Next

        Dim servicio As Service = Me.getServicio()
        servicio.updateRutas(rutas)

        Me.load_rutas()

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click

        If Preferences.validar() = False Then

            Preferences.Show()

            Return

        End If

        Dim usr As String = Me.tbLoginUsr.Text.Trim()
        Dim pass As String = Me.tbLoginPass.Text.Trim()

        Dim servicio As Service = Me.getServicio()

        Dim usuario As Usuario = servicio.getUsuarioByUsername(usr)

        If usuario._username <> usr Or usuario._password <> pass Then

            MessageBox.Show("Usuario y/ó contraseña non válida.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Else

            Me.CatalogosToolStripMenuItem.Visible = (Constants.CARGA_ROLE = (Constants.CARGA_ROLE And usuario._roles)) And _
                                                    (Constants.PUERTA_ROLE = (Constants.PUERTA_ROLE And usuario._roles)) And _
                                                    (Constants.CAJA_ROLE = (Constants.CAJA_ROLE And usuario._roles)) And _
                                                    (Constants.REGRESO_ROLE = (Constants.REGRESO_ROLE And usuario._roles))
            Me.ModulosToolStripMenuItem.Visible = (Constants.CARGA_ROLE = (Constants.CARGA_ROLE And usuario._roles)) Or _
                                                    (Constants.PUERTA_ROLE = (Constants.PUERTA_ROLE And usuario._roles)) Or _
                                                    (Constants.CAJA_ROLE = (Constants.CAJA_ROLE And usuario._roles)) Or _
                                                    (Constants.REGRESO_ROLE = (Constants.REGRESO_ROLE And usuario._roles))

            Me.CargaToolStripMenuItem.Visible = (Constants.CARGA_ROLE = (Constants.CARGA_ROLE And usuario._roles))
            Me.CajaToolStripMenuItem.Visible = (Constants.CAJA_ROLE = (Constants.CAJA_ROLE And usuario._roles))
            Me.PuertaToolStripMenuItem.Visible = (Constants.PUERTA_ROLE = (Constants.PUERTA_ROLE And usuario._roles))
            Me.RegresoToolStripMenuItem.Visible = (Constants.REGRESO_ROLE = (Constants.REGRESO_ROLE And usuario._roles))

            Me.hidePanels()

            If (Constants.CARGA_ROLE = (Constants.CARGA_ROLE And usuario._roles)) Then

                cargaPanel.Visible = True
                Me.limpiarCarga()
                Me.tbIdCarga.Focus()

            End If

            If (Constants.PUERTA_ROLE = (Constants.PUERTA_ROLE And usuario._roles)) Then

                puertaPanel.Visible = True
                Me.limpiarPuerta()
                Me.tbRutaPuerta.Focus()

            End If

            If (Constants.REGRESO_ROLE = (Constants.REGRESO_ROLE And usuario._roles)) Then

                regresoPanel.Visible = True
                Me.limpiarRegreso()
                Me.tbIdRegreso.Focus()

            End If

            If (Constants.CAJA_ROLE = (Constants.CAJA_ROLE And usuario._roles)) Then

                cajaPanel.Visible = True
                Me.limpiarCaja()
                Me.tbIdCaja.Focus()

            End If

        End If

    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosToolStripMenuItem.Click

        Me.load_usuarios()
        Me.hidePanels()
        Me.usuarioPanel.Visible = True

        Me.cbUsuarios.Focus()

    End Sub

    Private Sub load_usuarios()

        Me.cbUsuarios.Items.Clear()
       
        Dim servicio As Service = Me.getServicio()
        Dim usuarios As List(Of Usuario) = servicio.getUsuarios()
        Dim usernames As List(Of String) = New List(Of String)

        For Each user As Usuario In usuarios
            usernames.Add(user._username)
        Next

        Me.cbUsuarios.Items.AddRange(usernames.ToArray)

    End Sub

    Private Sub limpiarInformacionUsuario()

        Me.tbSecPassword.Text = String.Empty
        Me.tbUserName.Text = String.Empty

        Me.chkbCaja.Checked = False
        Me.chkbCarga.Checked = False
        Me.chkbPuerta.Checked = False
        Me.chkbRegreso.Checked = False

        Me.cbUsuarios.SelectedIndex = -1

        Me.tbUserName.ReadOnly = False

        Me.chkbCarga.Enabled = True
        Me.chkbPuerta.Enabled = True
        Me.chkbCaja.Enabled = True
        Me.chkbRegreso.Enabled = True

    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click

        If Me.cbUsuarios.SelectedIndex = -1 Then

            Return

        End If

        Dim username = Me.cbUsuarios.Text.Trim()

        If "admin" = username Then

            MessageBox.Show("El usuario admin no se puede eliminar, sólo se puede cambiar su contraseña.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Return

        End If

        Dim servicio As Service = Me.getServicio()

        Dim usuario As Usuario = servicio.getUsuarioByUsername(username)
        servicio.deleteUsuario(usuario)

        Me.limpiarInformacionUsuario()
        Me.load_usuarios()

        MessageBox.Show("Usuario eliminado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)


    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Me.limpiarInformacionUsuario()
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click

        Dim username As String = Me.tbUserName.Text.Trim()
        Dim password As String = Me.tbSecPassword.Text.Trim()

        Dim roles = &H0

        If Me.chkbCarga.Checked Then

            roles = roles Or Constants.CARGA_ROLE

        End If

        If Me.chkbRegreso.Checked Then

            roles = roles Or Constants.REGRESO_ROLE

        End If

        If Me.chkbPuerta.Checked Then

            roles = roles Or Constants.PUERTA_ROLE

        End If

        If Me.chkbCaja.Checked Then

            roles = roles Or Constants.CAJA_ROLE

        End If


        If Me.cbUsuarios.SelectedIndex = -1 Then

            If String.Empty = username Or String.Empty = password Or (0 = roles) Then

                MessageBox.Show("Ingrese el nombre de usuario, la contraseña y los roles aplicables.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return

            Else

                Dim servicio As Service = Me.getServicio()
                Dim existe As Boolean = servicio.existsUsuario(username)

                If existe Then

                    MessageBox.Show("El nombre de usuario ya existe", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    Return

                Else

                    Dim usuario As Usuario = New Usuario()
                    usuario._username = username
                    usuario._password = password
                    usuario._roles = roles

                    servicio.insertUsuario(usuario)

                    MessageBox.Show("Usuario creado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    Me.limpiarInformacionUsuario()
                    Me.load_usuarios()

                End If

            End If

        Else

            Dim servicio As Service = Me.getServicio()
            Dim usuario As Usuario = servicio.getUsuarioByUsername(Me.cbUsuarios.Text.Trim())

            usuario._username = username
            usuario._password = password
            usuario._roles = roles

            servicio.updateUsuario(usuario)

            MessageBox.Show("Usuario actualizado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Me.limpiarInformacionUsuario()
                Me.load_usuarios()

        End If

    End Sub

    Private Sub cbUsuarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbUsuarios.SelectedIndexChanged

        If Me.cbUsuarios.SelectedIndex = -1 Then

            Me.limpiarInformacionUsuario()
            Return

        End If

        Dim username = Me.cbUsuarios.Text.Trim()

        Dim servicio As Service = Me.getServicio()

        Dim usuario As Usuario = servicio.getUsuarioByUsername(username)

        Me.tbSecPassword.Text = usuario._password
        Me.tbUserName.Text = usuario._username

        Me.chkbCarga.Checked = (Constants.CARGA_ROLE = (Constants.CARGA_ROLE And usuario._roles))
        Me.chkbPuerta.Checked = (Constants.PUERTA_ROLE = (Constants.PUERTA_ROLE And usuario._roles))
        Me.chkbCaja.Checked = (Constants.CAJA_ROLE = (Constants.CAJA_ROLE And usuario._roles))
        Me.chkbRegreso.Checked = (Constants.REGRESO_ROLE = (Constants.REGRESO_ROLE And usuario._roles))

        If "admin" = username Then

            Me.tbUserName.ReadOnly = True

            Me.chkbCarga.Enabled = False
            Me.chkbPuerta.Enabled = False
            Me.chkbCaja.Enabled = False
            Me.chkbRegreso.Enabled = False

        End If

    End Sub


End Class
