﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Super Agua")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Grupo Slash Core S.A. de C. V.")> 
<Assembly: AssemblyProduct("SuperAguaApp")> 
<Assembly: AssemblyCopyright("Copyright © 2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("0cc22e58-f175-4573-ae6d-694f47311368")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.3")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
