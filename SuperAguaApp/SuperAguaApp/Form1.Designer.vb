﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HerramientasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguraciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatalogosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RutasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModulosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuertaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegresoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcercaDeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnBuscarCarga = New System.Windows.Forms.Button()
        Me.btnGuardarCarga = New System.Windows.Forms.Button()
        Me.tbFechaCarga = New System.Windows.Forms.TextBox()
        Me.fechaLabel = New System.Windows.Forms.Label()
        Me.tbCantidadCarga = New System.Windows.Forms.TextBox()
        Me.cantidadLabel = New System.Windows.Forms.Label()
        Me.tbIdCarga = New System.Windows.Forms.TextBox()
        Me.idLabel = New System.Windows.Forms.Label()
        Me.puertaPanel = New System.Windows.Forms.Panel()
        Me.tbEnvasePuerta = New System.Windows.Forms.TextBox()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.tbRutaPuerta = New System.Windows.Forms.ComboBox()
        Me.tbObsPuerta = New System.Windows.Forms.TextBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.tbIdPuerta = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.tbCaja1000Puerta = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.tbCaja1500Puerta = New System.Windows.Forms.TextBox()
        Me.tbCaja500Puerta = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.tbCaja355Puerta = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbLlavesPuerta = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btnBuscarPuerta = New System.Windows.Forms.Button()
        Me.btnGuardarPuerta = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbHieloPuerta = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tbDestiladoPuerta = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbLiquidoPuerta = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnLimpiarPuerta = New System.Windows.Forms.Button()
        Me.regresoPanel = New System.Windows.Forms.Panel()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.tbCaja1000Regreso = New System.Windows.Forms.TextBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.tbCaja355Regreso = New System.Windows.Forms.TextBox()
        Me.tbCaja1500Regreso = New System.Windows.Forms.TextBox()
        Me.tbCaja500Regreso = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.btnLimpiarRegreso = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tbHieloRegreso = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.tbRecuperadosRegreso = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tbPrestamoRegreso = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbVentaRegreso = New System.Windows.Forms.TextBox()
        Me.tbLlavesRegreso = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.tbLlenosRegreso = New System.Windows.Forms.TextBox()
        Me.tbCompetenciaRegreso = New System.Windows.Forms.TextBox()
        Me.tbEnvaseRegreso = New System.Windows.Forms.TextBox()
        Me.tbRotosRegreso = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btnBuscarRegreso = New System.Windows.Forms.Button()
        Me.btnGuardarRegreso = New System.Windows.Forms.Button()
        Me.tbObsRegreso = New System.Windows.Forms.TextBox()
        Me.tbIdRegreso = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.rutaLabel = New System.Windows.Forms.Label()
        Me.btnLimpiarCarga = New System.Windows.Forms.Button()
        Me.cajaPanel = New System.Windows.Forms.Panel()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.iTotalHielo = New System.Windows.Forms.Label()
        Me.lTotalCaja = New System.Windows.Forms.Label()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.tbEnvasePuertaCaja = New System.Windows.Forms.TextBox()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.tbObsPuertaCaja = New System.Windows.Forms.TextBox()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.tb1000PuertaCaja = New System.Windows.Forms.TextBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.tb1500PuertaCaja = New System.Windows.Forms.TextBox()
        Me.tb500PuertaCaja = New System.Windows.Forms.TextBox()
        Me.tb355PuertaCaja = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.tbLlavesPuertaCaja = New System.Windows.Forms.TextBox()
        Me.tbHieloPuertaCaja = New System.Windows.Forms.TextBox()
        Me.tbDestiladoPuertaCaja = New System.Windows.Forms.TextBox()
        Me.tbLiquidoPuertaCaja = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.btnGuardarCaja = New System.Windows.Forms.Button()
        Me.btnLimpiarCaja = New System.Windows.Forms.Button()
        Me.btnBuscarCaja = New System.Windows.Forms.Button()
        Me.cbCerradaCaja = New System.Windows.Forms.CheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.tbDif1500 = New System.Windows.Forms.TextBox()
        Me.tbDif1000 = New System.Windows.Forms.TextBox()
        Me.tbDif500 = New System.Windows.Forms.TextBox()
        Me.tbDif355 = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.tbObsRegresoCaja = New System.Windows.Forms.TextBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.tb1000RegresoCaja = New System.Windows.Forms.TextBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.tb1500RegresoCaja = New System.Windows.Forms.TextBox()
        Me.tb500RegresoCaja = New System.Windows.Forms.TextBox()
        Me.tb355RegresoCaja = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.tbRecuperadosRegresoCaja = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.tbHieloRegresoCaja = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.tbPrestamoRegresoCaja = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.tbVentRegresoCaja = New System.Windows.Forms.TextBox()
        Me.tbLlavesRegresoCaja = New System.Windows.Forms.TextBox()
        Me.tbLlenosRegresoCaja = New System.Windows.Forms.TextBox()
        Me.tbCompetenciaRegresoCaja = New System.Windows.Forms.TextBox()
        Me.tbEnvaseRegresoCaja = New System.Windows.Forms.TextBox()
        Me.tbRotosRegresoCaja = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.tbEnvaseCargaCaja = New System.Windows.Forms.TextBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.tb1000CargaCaja = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.tb1500CargaCaja = New System.Windows.Forms.TextBox()
        Me.tb500CargaCaja = New System.Windows.Forms.TextBox()
        Me.tb355CargaCaja = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbHieloCargaCaja = New System.Windows.Forms.TextBox()
        Me.tbDestiladoCargaCaja = New System.Windows.Forms.TextBox()
        Me.tbLiquidoCargaCaja = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.tbIdCaja = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.cargaPanel = New System.Windows.Forms.Panel()
        Me.tbEnvaseCarga = New System.Windows.Forms.TextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.tbRutaCarga = New System.Windows.Forms.ComboBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.tbCaja1500Carga = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.tbCaja1000Carga = New System.Windows.Forms.TextBox()
        Me.tbCaja500Carga = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbCaja355Carga = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbHieloCarga = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.tbDestiladoCarga = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.tbLiquidoCarga = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.rutaPanel = New System.Windows.Forms.Panel()
        Me.GroupBox15 = New System.Windows.Forms.GroupBox()
        Me.lbRuta = New System.Windows.Forms.ListBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.cbRutas = New System.Windows.Forms.ComboBox()
        Me.tbRutaCatalogo = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.loginPanel = New System.Windows.Forms.Panel()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.tbLoginPass = New System.Windows.Forms.TextBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.tbLoginUsr = New System.Windows.Forms.TextBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.usuarioPanel = New System.Windows.Forms.Panel()
        Me.Detalle = New System.Windows.Forms.GroupBox()
        Me.tbUserName = New System.Windows.Forms.TextBox()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.cbUsuarios = New System.Windows.Forms.ComboBox()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.tbSecPassword = New System.Windows.Forms.TextBox()
        Me.chkbCaja = New System.Windows.Forms.CheckBox()
        Me.chkbRegreso = New System.Windows.Forms.CheckBox()
        Me.chkbPuerta = New System.Windows.Forms.CheckBox()
        Me.chkbCarga = New System.Windows.Forms.CheckBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.puertaPanel.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.regresoPanel.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.cajaPanel.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.cargaPanel.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.rutaPanel.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.loginPanel.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.usuarioPanel.SuspendLayout()
        Me.Detalle.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.HerramientasToolStripMenuItem, Me.ModulosToolStripMenuItem, Me.AyudaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(996, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SalirToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "Archivo"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(96, 22)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'HerramientasToolStripMenuItem
        '
        Me.HerramientasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConfiguraciónToolStripMenuItem, Me.CatalogosToolStripMenuItem})
        Me.HerramientasToolStripMenuItem.Name = "HerramientasToolStripMenuItem"
        Me.HerramientasToolStripMenuItem.Size = New System.Drawing.Size(90, 20)
        Me.HerramientasToolStripMenuItem.Text = "Herramientas"
        '
        'ConfiguraciónToolStripMenuItem
        '
        Me.ConfiguraciónToolStripMenuItem.Name = "ConfiguraciónToolStripMenuItem"
        Me.ConfiguraciónToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.ConfiguraciónToolStripMenuItem.Text = "Configuración"
        '
        'CatalogosToolStripMenuItem
        '
        Me.CatalogosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RutasToolStripMenuItem, Me.UsuariosToolStripMenuItem})
        Me.CatalogosToolStripMenuItem.Name = "CatalogosToolStripMenuItem"
        Me.CatalogosToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.CatalogosToolStripMenuItem.Text = "Catálogos"
        '
        'RutasToolStripMenuItem
        '
        Me.RutasToolStripMenuItem.Name = "RutasToolStripMenuItem"
        Me.RutasToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.RutasToolStripMenuItem.Text = "Rutas"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'ModulosToolStripMenuItem
        '
        Me.ModulosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem, Me.PuertaToolStripMenuItem, Me.RegresoToolStripMenuItem, Me.CajaToolStripMenuItem})
        Me.ModulosToolStripMenuItem.Name = "ModulosToolStripMenuItem"
        Me.ModulosToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.ModulosToolStripMenuItem.Text = "Módulos"
        '
        'CargaToolStripMenuItem
        '
        Me.CargaToolStripMenuItem.Name = "CargaToolStripMenuItem"
        Me.CargaToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.CargaToolStripMenuItem.Text = "Carga"
        '
        'PuertaToolStripMenuItem
        '
        Me.PuertaToolStripMenuItem.Name = "PuertaToolStripMenuItem"
        Me.PuertaToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.PuertaToolStripMenuItem.Text = "Puerta"
        '
        'RegresoToolStripMenuItem
        '
        Me.RegresoToolStripMenuItem.Name = "RegresoToolStripMenuItem"
        Me.RegresoToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.RegresoToolStripMenuItem.Text = "Regreso"
        '
        'CajaToolStripMenuItem
        '
        Me.CajaToolStripMenuItem.Name = "CajaToolStripMenuItem"
        Me.CajaToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.CajaToolStripMenuItem.Text = "Caja"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AcercaDeToolStripMenuItem})
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.AyudaToolStripMenuItem.Text = "Ayuda"
        '
        'AcercaDeToolStripMenuItem
        '
        Me.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem"
        Me.AcercaDeToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.AcercaDeToolStripMenuItem.Text = "Acerca de"
        '
        'btnBuscarCarga
        '
        Me.btnBuscarCarga.Location = New System.Drawing.Point(356, 179)
        Me.btnBuscarCarga.Name = "btnBuscarCarga"
        Me.btnBuscarCarga.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscarCarga.TabIndex = 1
        Me.btnBuscarCarga.Text = "Buscar"
        Me.btnBuscarCarga.UseVisualStyleBackColor = True
        '
        'btnGuardarCarga
        '
        Me.btnGuardarCarga.Location = New System.Drawing.Point(661, 374)
        Me.btnGuardarCarga.Name = "btnGuardarCarga"
        Me.btnGuardarCarga.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardarCarga.TabIndex = 13
        Me.btnGuardarCarga.Text = "Guardar"
        Me.btnGuardarCarga.UseVisualStyleBackColor = True
        '
        'tbFechaCarga
        '
        Me.tbFechaCarga.Location = New System.Drawing.Point(262, 286)
        Me.tbFechaCarga.Name = "tbFechaCarga"
        Me.tbFechaCarga.ReadOnly = True
        Me.tbFechaCarga.Size = New System.Drawing.Size(114, 20)
        Me.tbFechaCarga.TabIndex = 15
        '
        'fechaLabel
        '
        Me.fechaLabel.AutoSize = True
        Me.fechaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fechaLabel.Location = New System.Drawing.Point(200, 289)
        Me.fechaLabel.Name = "fechaLabel"
        Me.fechaLabel.Size = New System.Drawing.Size(46, 13)
        Me.fechaLabel.TabIndex = 1000
        Me.fechaLabel.Text = "Fecha:"
        '
        'tbCantidadCarga
        '
        Me.tbCantidadCarga.Location = New System.Drawing.Point(262, 261)
        Me.tbCantidadCarga.Name = "tbCantidadCarga"
        Me.tbCantidadCarga.ReadOnly = True
        Me.tbCantidadCarga.Size = New System.Drawing.Size(114, 20)
        Me.tbCantidadCarga.TabIndex = 14
        Me.tbCantidadCarga.Text = "0"
        Me.tbCantidadCarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cantidadLabel
        '
        Me.cantidadLabel.AutoSize = True
        Me.cantidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cantidadLabel.Location = New System.Drawing.Point(199, 264)
        Me.cantidadLabel.Name = "cantidadLabel"
        Me.cantidadLabel.Size = New System.Drawing.Size(61, 13)
        Me.cantidadLabel.TabIndex = 1000
        Me.cantidadLabel.Text = "Cantidad:"
        '
        'tbIdCarga
        '
        Me.tbIdCarga.Location = New System.Drawing.Point(238, 181)
        Me.tbIdCarga.Name = "tbIdCarga"
        Me.tbIdCarga.Size = New System.Drawing.Size(100, 20)
        Me.tbIdCarga.TabIndex = 0
        Me.tbIdCarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'idLabel
        '
        Me.idLabel.AutoSize = True
        Me.idLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.idLabel.Location = New System.Drawing.Point(203, 185)
        Me.idLabel.Name = "idLabel"
        Me.idLabel.Size = New System.Drawing.Size(22, 13)
        Me.idLabel.TabIndex = 1000
        Me.idLabel.Text = "Id:"
        '
        'puertaPanel
        '
        Me.puertaPanel.Controls.Add(Me.tbEnvasePuerta)
        Me.puertaPanel.Controls.Add(Me.Label73)
        Me.puertaPanel.Controls.Add(Me.tbRutaPuerta)
        Me.puertaPanel.Controls.Add(Me.tbObsPuerta)
        Me.puertaPanel.Controls.Add(Me.Label71)
        Me.puertaPanel.Controls.Add(Me.Label68)
        Me.puertaPanel.Controls.Add(Me.tbIdPuerta)
        Me.puertaPanel.Controls.Add(Me.Label23)
        Me.puertaPanel.Controls.Add(Me.GroupBox2)
        Me.puertaPanel.Controls.Add(Me.tbLlavesPuerta)
        Me.puertaPanel.Controls.Add(Me.Label20)
        Me.puertaPanel.Controls.Add(Me.btnBuscarPuerta)
        Me.puertaPanel.Controls.Add(Me.btnGuardarPuerta)
        Me.puertaPanel.Controls.Add(Me.Label10)
        Me.puertaPanel.Controls.Add(Me.tbHieloPuerta)
        Me.puertaPanel.Controls.Add(Me.Label9)
        Me.puertaPanel.Controls.Add(Me.tbDestiladoPuerta)
        Me.puertaPanel.Controls.Add(Me.Label7)
        Me.puertaPanel.Controls.Add(Me.tbLiquidoPuerta)
        Me.puertaPanel.Controls.Add(Me.Label6)
        Me.puertaPanel.Controls.Add(Me.btnLimpiarPuerta)
        Me.puertaPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.puertaPanel.Location = New System.Drawing.Point(0, 24)
        Me.puertaPanel.Name = "puertaPanel"
        Me.puertaPanel.Size = New System.Drawing.Size(996, 613)
        Me.puertaPanel.TabIndex = 24
        Me.puertaPanel.Visible = False
        '
        'tbEnvasePuerta
        '
        Me.tbEnvasePuerta.Location = New System.Drawing.Point(467, 204)
        Me.tbEnvasePuerta.Name = "tbEnvasePuerta"
        Me.tbEnvasePuerta.Size = New System.Drawing.Size(100, 20)
        Me.tbEnvasePuerta.TabIndex = 3
        Me.tbEnvasePuerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.Location = New System.Drawing.Point(399, 207)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(53, 13)
        Me.Label73.TabIndex = 1000
        Me.Label73.Text = "Envase:"
        '
        'tbRutaPuerta
        '
        Me.tbRutaPuerta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.tbRutaPuerta.FormattingEnabled = True
        Me.tbRutaPuerta.Location = New System.Drawing.Point(250, 157)
        Me.tbRutaPuerta.Name = "tbRutaPuerta"
        Me.tbRutaPuerta.Size = New System.Drawing.Size(100, 21)
        Me.tbRutaPuerta.TabIndex = 0
        '
        'tbObsPuerta
        '
        Me.tbObsPuerta.Location = New System.Drawing.Point(487, 313)
        Me.tbObsPuerta.Multiline = True
        Me.tbObsPuerta.Name = "tbObsPuerta"
        Me.tbObsPuerta.Size = New System.Drawing.Size(276, 96)
        Me.tbObsPuerta.TabIndex = 11
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.Location = New System.Drawing.Point(393, 312)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(95, 13)
        Me.Label71.TabIndex = 1000
        Me.Label71.Text = "Observaciones:"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label68.Location = New System.Drawing.Point(805, 20)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(175, 24)
        Me.Label68.TabIndex = 23
        Me.Label68.Text = "Módulo de puerta"
        '
        'tbIdPuerta
        '
        Me.tbIdPuerta.Location = New System.Drawing.Point(250, 187)
        Me.tbIdPuerta.Name = "tbIdPuerta"
        Me.tbIdPuerta.Size = New System.Drawing.Size(100, 20)
        Me.tbIdPuerta.TabIndex = 15
        Me.tbIdPuerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(211, 191)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(22, 13)
        Me.Label23.TabIndex = 1000
        Me.Label23.Text = "Id:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.tbCaja1000Puerta)
        Me.GroupBox2.Controls.Add(Me.Label59)
        Me.GroupBox2.Controls.Add(Me.tbCaja1500Puerta)
        Me.GroupBox2.Controls.Add(Me.tbCaja500Puerta)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.tbCaja355Puerta)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(582, 148)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(179, 148)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Caja"
        '
        'tbCaja1000Puerta
        '
        Me.tbCaja1000Puerta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja1000Puerta.Location = New System.Drawing.Point(65, 81)
        Me.tbCaja1000Puerta.Name = "tbCaja1000Puerta"
        Me.tbCaja1000Puerta.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja1000Puerta.TabIndex = 9
        Me.tbCaja1000Puerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(15, 110)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(51, 13)
        Me.Label59.TabIndex = 1000
        Me.Label59.Text = "1500ml:"
        '
        'tbCaja1500Puerta
        '
        Me.tbCaja1500Puerta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja1500Puerta.Location = New System.Drawing.Point(65, 107)
        Me.tbCaja1500Puerta.Name = "tbCaja1500Puerta"
        Me.tbCaja1500Puerta.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja1500Puerta.TabIndex = 10
        Me.tbCaja1500Puerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbCaja500Puerta
        '
        Me.tbCaja500Puerta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja500Puerta.Location = New System.Drawing.Point(65, 56)
        Me.tbCaja500Puerta.Name = "tbCaja500Puerta"
        Me.tbCaja500Puerta.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja500Puerta.TabIndex = 8
        Me.tbCaja500Puerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(15, 85)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(51, 13)
        Me.Label22.TabIndex = 1000
        Me.Label22.Text = "1000ml:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(15, 59)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(44, 13)
        Me.Label21.TabIndex = 1000
        Me.Label21.Text = "500ml:"
        '
        'tbCaja355Puerta
        '
        Me.tbCaja355Puerta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja355Puerta.Location = New System.Drawing.Point(65, 31)
        Me.tbCaja355Puerta.Name = "tbCaja355Puerta"
        Me.tbCaja355Puerta.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja355Puerta.TabIndex = 7
        Me.tbCaja355Puerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 34)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 13)
        Me.Label8.TabIndex = 1000
        Me.Label8.Text = "355ml:"
        '
        'tbLlavesPuerta
        '
        Me.tbLlavesPuerta.Location = New System.Drawing.Point(467, 253)
        Me.tbLlavesPuerta.Name = "tbLlavesPuerta"
        Me.tbLlavesPuerta.Size = New System.Drawing.Size(100, 20)
        Me.tbLlavesPuerta.TabIndex = 5
        Me.tbLlavesPuerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(398, 256)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(48, 13)
        Me.Label20.TabIndex = 1000
        Me.Label20.Text = "Llaves:"
        '
        'btnBuscarPuerta
        '
        Me.btnBuscarPuerta.Location = New System.Drawing.Point(595, 443)
        Me.btnBuscarPuerta.Name = "btnBuscarPuerta"
        Me.btnBuscarPuerta.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscarPuerta.TabIndex = 13
        Me.btnBuscarPuerta.Text = "Buscar"
        Me.btnBuscarPuerta.UseVisualStyleBackColor = True
        '
        'btnGuardarPuerta
        '
        Me.btnGuardarPuerta.Location = New System.Drawing.Point(687, 443)
        Me.btnGuardarPuerta.Name = "btnGuardarPuerta"
        Me.btnGuardarPuerta.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardarPuerta.TabIndex = 14
        Me.btnGuardarPuerta.Text = "Guardar"
        Me.btnGuardarPuerta.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(211, 161)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 13)
        Me.Label10.TabIndex = 1000
        Me.Label10.Text = "Ruta:"
        '
        'tbHieloPuerta
        '
        Me.tbHieloPuerta.Location = New System.Drawing.Point(467, 229)
        Me.tbHieloPuerta.Name = "tbHieloPuerta"
        Me.tbHieloPuerta.Size = New System.Drawing.Size(100, 20)
        Me.tbHieloPuerta.TabIndex = 4
        Me.tbHieloPuerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(398, 231)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 13)
        Me.Label9.TabIndex = 1000
        Me.Label9.Text = "Hielo:"
        '
        'tbDestiladoPuerta
        '
        Me.tbDestiladoPuerta.Location = New System.Drawing.Point(467, 180)
        Me.tbDestiladoPuerta.Name = "tbDestiladoPuerta"
        Me.tbDestiladoPuerta.Size = New System.Drawing.Size(100, 20)
        Me.tbDestiladoPuerta.TabIndex = 2
        Me.tbDestiladoPuerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(398, 184)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 13)
        Me.Label7.TabIndex = 1000
        Me.Label7.Text = "Destilado:"
        '
        'tbLiquidoPuerta
        '
        Me.tbLiquidoPuerta.Location = New System.Drawing.Point(467, 156)
        Me.tbLiquidoPuerta.Name = "tbLiquidoPuerta"
        Me.tbLiquidoPuerta.Size = New System.Drawing.Size(100, 20)
        Me.tbLiquidoPuerta.TabIndex = 1
        Me.tbLiquidoPuerta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(398, 158)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 13)
        Me.Label6.TabIndex = 1000
        Me.Label6.Text = "Líquido:"
        '
        'btnLimpiarPuerta
        '
        Me.btnLimpiarPuerta.Location = New System.Drawing.Point(506, 443)
        Me.btnLimpiarPuerta.Name = "btnLimpiarPuerta"
        Me.btnLimpiarPuerta.Size = New System.Drawing.Size(75, 23)
        Me.btnLimpiarPuerta.TabIndex = 12
        Me.btnLimpiarPuerta.Text = "Limpiar"
        Me.btnLimpiarPuerta.UseVisualStyleBackColor = True
        '
        'regresoPanel
        '
        Me.regresoPanel.Controls.Add(Me.Label70)
        Me.regresoPanel.Controls.Add(Me.GroupBox3)
        Me.regresoPanel.Controls.Add(Me.btnLimpiarRegreso)
        Me.regresoPanel.Controls.Add(Me.GroupBox1)
        Me.regresoPanel.Controls.Add(Me.btnBuscarRegreso)
        Me.regresoPanel.Controls.Add(Me.btnGuardarRegreso)
        Me.regresoPanel.Controls.Add(Me.tbObsRegreso)
        Me.regresoPanel.Controls.Add(Me.tbIdRegreso)
        Me.regresoPanel.Controls.Add(Me.Label15)
        Me.regresoPanel.Controls.Add(Me.Label11)
        Me.regresoPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.regresoPanel.Location = New System.Drawing.Point(0, 24)
        Me.regresoPanel.Name = "regresoPanel"
        Me.regresoPanel.Size = New System.Drawing.Size(996, 613)
        Me.regresoPanel.TabIndex = 12
        Me.regresoPanel.Visible = False
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label70.Location = New System.Drawing.Point(791, 26)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(188, 24)
        Me.Label70.TabIndex = 40
        Me.Label70.Text = "Módulo de regreso"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.tbCaja1000Regreso)
        Me.GroupBox3.Controls.Add(Me.Label61)
        Me.GroupBox3.Controls.Add(Me.tbCaja355Regreso)
        Me.GroupBox3.Controls.Add(Me.tbCaja1500Regreso)
        Me.GroupBox3.Controls.Add(Me.tbCaja500Regreso)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.Label25)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(509, 106)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(200, 288)
        Me.GroupBox3.TabIndex = 11
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Caja"
        '
        'tbCaja1000Regreso
        '
        Me.tbCaja1000Regreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja1000Regreso.Location = New System.Drawing.Point(84, 81)
        Me.tbCaja1000Regreso.Name = "tbCaja1000Regreso"
        Me.tbCaja1000Regreso.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja1000Regreso.TabIndex = 14
        Me.tbCaja1000Regreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(23, 90)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(51, 13)
        Me.Label61.TabIndex = 1000
        Me.Label61.Text = "1000ml:"
        '
        'tbCaja355Regreso
        '
        Me.tbCaja355Regreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja355Regreso.Location = New System.Drawing.Point(84, 26)
        Me.tbCaja355Regreso.Name = "tbCaja355Regreso"
        Me.tbCaja355Regreso.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja355Regreso.TabIndex = 12
        Me.tbCaja355Regreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbCaja1500Regreso
        '
        Me.tbCaja1500Regreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja1500Regreso.Location = New System.Drawing.Point(84, 111)
        Me.tbCaja1500Regreso.Name = "tbCaja1500Regreso"
        Me.tbCaja1500Regreso.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja1500Regreso.TabIndex = 15
        Me.tbCaja1500Regreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbCaja500Regreso
        '
        Me.tbCaja500Regreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja500Regreso.Location = New System.Drawing.Point(84, 52)
        Me.tbCaja500Regreso.Name = "tbCaja500Regreso"
        Me.tbCaja500Regreso.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja500Regreso.TabIndex = 13
        Me.tbCaja500Regreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(23, 115)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(51, 13)
        Me.Label26.TabIndex = 1000
        Me.Label26.Text = "1500ml:"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(23, 59)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(44, 13)
        Me.Label25.TabIndex = 1000
        Me.Label25.Text = "500ml:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(23, 30)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(44, 13)
        Me.Label24.TabIndex = 1000
        Me.Label24.Text = "355ml:"
        '
        'btnLimpiarRegreso
        '
        Me.btnLimpiarRegreso.Location = New System.Drawing.Point(543, 530)
        Me.btnLimpiarRegreso.Name = "btnLimpiarRegreso"
        Me.btnLimpiarRegreso.Size = New System.Drawing.Size(75, 23)
        Me.btnLimpiarRegreso.TabIndex = 17
        Me.btnLimpiarRegreso.Text = "Limpiar"
        Me.btnLimpiarRegreso.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tbHieloRegreso)
        Me.GroupBox1.Controls.Add(Me.Label49)
        Me.GroupBox1.Controls.Add(Me.tbRecuperadosRegreso)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.tbPrestamoRegreso)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.tbVentaRegreso)
        Me.GroupBox1.Controls.Add(Me.tbLlavesRegreso)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.tbLlenosRegreso)
        Me.GroupBox1.Controls.Add(Me.tbCompetenciaRegreso)
        Me.GroupBox1.Controls.Add(Me.tbEnvaseRegreso)
        Me.GroupBox1.Controls.Add(Me.tbRotosRegreso)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(250, 102)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(253, 292)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Envases:"
        '
        'tbHieloRegreso
        '
        Me.tbHieloRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbHieloRegreso.Location = New System.Drawing.Point(135, 214)
        Me.tbHieloRegreso.Name = "tbHieloRegreso"
        Me.tbHieloRegreso.Size = New System.Drawing.Size(100, 20)
        Me.tbHieloRegreso.TabIndex = 10
        Me.tbHieloRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(35, 217)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(40, 13)
        Me.Label49.TabIndex = 1000
        Me.Label49.Text = "Hielo:"
        '
        'tbRecuperadosRegreso
        '
        Me.tbRecuperadosRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRecuperadosRegreso.Location = New System.Drawing.Point(134, 158)
        Me.tbRecuperadosRegreso.Name = "tbRecuperadosRegreso"
        Me.tbRecuperadosRegreso.Size = New System.Drawing.Size(100, 20)
        Me.tbRecuperadosRegreso.TabIndex = 8
        Me.tbRecuperadosRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(33, 162)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(86, 13)
        Me.Label17.TabIndex = 1000
        Me.Label17.Text = "Recuperados:"
        '
        'tbPrestamoRegreso
        '
        Me.tbPrestamoRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPrestamoRegreso.Location = New System.Drawing.Point(134, 187)
        Me.tbPrestamoRegreso.Name = "tbPrestamoRegreso"
        Me.tbPrestamoRegreso.Size = New System.Drawing.Size(100, 20)
        Me.tbPrestamoRegreso.TabIndex = 9
        Me.tbPrestamoRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(35, 191)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 1000
        Me.Label4.Text = "Prestamo:"
        '
        'tbVentaRegreso
        '
        Me.tbVentaRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbVentaRegreso.Location = New System.Drawing.Point(134, 243)
        Me.tbVentaRegreso.Name = "tbVentaRegreso"
        Me.tbVentaRegreso.ReadOnly = True
        Me.tbVentaRegreso.Size = New System.Drawing.Size(99, 20)
        Me.tbVentaRegreso.TabIndex = 19
        Me.tbVentaRegreso.Text = "0"
        Me.tbVentaRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbLlavesRegreso
        '
        Me.tbLlavesRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLlavesRegreso.Location = New System.Drawing.Point(135, 132)
        Me.tbLlavesRegreso.Name = "tbLlavesRegreso"
        Me.tbLlavesRegreso.Size = New System.Drawing.Size(100, 20)
        Me.tbLlavesRegreso.TabIndex = 7
        Me.tbLlavesRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(34, 243)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(89, 13)
        Me.Label19.TabIndex = 1000
        Me.Label19.Text = "Total de viaje:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(35, 136)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(44, 13)
        Me.Label18.TabIndex = 1000
        Me.Label18.Text = "Llaves"
        '
        'tbLlenosRegreso
        '
        Me.tbLlenosRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLlenosRegreso.Location = New System.Drawing.Point(135, 106)
        Me.tbLlenosRegreso.Name = "tbLlenosRegreso"
        Me.tbLlenosRegreso.Size = New System.Drawing.Size(100, 20)
        Me.tbLlenosRegreso.TabIndex = 6
        Me.tbLlenosRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbCompetenciaRegreso
        '
        Me.tbCompetenciaRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCompetenciaRegreso.Location = New System.Drawing.Point(135, 80)
        Me.tbCompetenciaRegreso.Name = "tbCompetenciaRegreso"
        Me.tbCompetenciaRegreso.Size = New System.Drawing.Size(100, 20)
        Me.tbCompetenciaRegreso.TabIndex = 5
        Me.tbCompetenciaRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbEnvaseRegreso
        '
        Me.tbEnvaseRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEnvaseRegreso.Location = New System.Drawing.Point(135, 53)
        Me.tbEnvaseRegreso.Name = "tbEnvaseRegreso"
        Me.tbEnvaseRegreso.Size = New System.Drawing.Size(100, 20)
        Me.tbEnvaseRegreso.TabIndex = 4
        Me.tbEnvaseRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbRotosRegreso
        '
        Me.tbRotosRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRotosRegreso.Location = New System.Drawing.Point(134, 27)
        Me.tbRotosRegreso.Name = "tbRotosRegreso"
        Me.tbRotosRegreso.Size = New System.Drawing.Size(100, 20)
        Me.tbRotosRegreso.TabIndex = 3
        Me.tbRotosRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(35, 30)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(44, 13)
        Me.Label16.TabIndex = 1000
        Me.Label16.Text = "Rotos:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(35, 109)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(48, 13)
        Me.Label14.TabIndex = 1000
        Me.Label14.Text = "Llenos:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(35, 83)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(84, 13)
        Me.Label13.TabIndex = 1000
        Me.Label13.Text = "Competencia:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(35, 56)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(53, 13)
        Me.Label12.TabIndex = 1000
        Me.Label12.Text = "Envase:"
        '
        'btnBuscarRegreso
        '
        Me.btnBuscarRegreso.Location = New System.Drawing.Point(416, 60)
        Me.btnBuscarRegreso.Name = "btnBuscarRegreso"
        Me.btnBuscarRegreso.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscarRegreso.TabIndex = 1
        Me.btnBuscarRegreso.Text = "Buscar"
        Me.btnBuscarRegreso.UseVisualStyleBackColor = True
        '
        'btnGuardarRegreso
        '
        Me.btnGuardarRegreso.Location = New System.Drawing.Point(634, 530)
        Me.btnGuardarRegreso.Name = "btnGuardarRegreso"
        Me.btnGuardarRegreso.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardarRegreso.TabIndex = 18
        Me.btnGuardarRegreso.Text = "Guardar"
        Me.btnGuardarRegreso.UseVisualStyleBackColor = True
        '
        'tbObsRegreso
        '
        Me.tbObsRegreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbObsRegreso.Location = New System.Drawing.Point(354, 403)
        Me.tbObsRegreso.Multiline = True
        Me.tbObsRegreso.Name = "tbObsRegreso"
        Me.tbObsRegreso.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.tbObsRegreso.Size = New System.Drawing.Size(344, 96)
        Me.tbObsRegreso.TabIndex = 16
        '
        'tbIdRegreso
        '
        Me.tbIdRegreso.Location = New System.Drawing.Point(299, 62)
        Me.tbIdRegreso.Name = "tbIdRegreso"
        Me.tbIdRegreso.Size = New System.Drawing.Size(100, 20)
        Me.tbIdRegreso.TabIndex = 0
        Me.tbIdRegreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(253, 407)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(95, 13)
        Me.Label15.TabIndex = 1000
        Me.Label15.Text = "Observaciones:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(249, 65)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(22, 13)
        Me.Label11.TabIndex = 1000
        Me.Label11.Text = "Id:"
        '
        'rutaLabel
        '
        Me.rutaLabel.AutoSize = True
        Me.rutaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rutaLabel.Location = New System.Drawing.Point(199, 240)
        Me.rutaLabel.Name = "rutaLabel"
        Me.rutaLabel.Size = New System.Drawing.Size(38, 13)
        Me.rutaLabel.TabIndex = 1000
        Me.rutaLabel.Text = "Ruta:"
        '
        'btnLimpiarCarga
        '
        Me.btnLimpiarCarga.Location = New System.Drawing.Point(575, 374)
        Me.btnLimpiarCarga.Name = "btnLimpiarCarga"
        Me.btnLimpiarCarga.Size = New System.Drawing.Size(75, 23)
        Me.btnLimpiarCarga.TabIndex = 12
        Me.btnLimpiarCarga.Text = "Limpiar"
        Me.btnLimpiarCarga.UseVisualStyleBackColor = True
        '
        'cajaPanel
        '
        Me.cajaPanel.Controls.Add(Me.Label44)
        Me.cajaPanel.Controls.Add(Me.GroupBox13)
        Me.cajaPanel.Controls.Add(Me.GroupBox10)
        Me.cajaPanel.Controls.Add(Me.btnGuardarCaja)
        Me.cajaPanel.Controls.Add(Me.btnLimpiarCaja)
        Me.cajaPanel.Controls.Add(Me.btnBuscarCaja)
        Me.cajaPanel.Controls.Add(Me.cbCerradaCaja)
        Me.cajaPanel.Controls.Add(Me.GroupBox5)
        Me.cajaPanel.Controls.Add(Me.GroupBox4)
        Me.cajaPanel.Controls.Add(Me.tbIdCaja)
        Me.cajaPanel.Controls.Add(Me.Label27)
        Me.cajaPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cajaPanel.Location = New System.Drawing.Point(0, 24)
        Me.cajaPanel.Name = "cajaPanel"
        Me.cajaPanel.Size = New System.Drawing.Size(996, 613)
        Me.cajaPanel.TabIndex = 30
        Me.cajaPanel.Visible = False
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label44.Location = New System.Drawing.Point(805, 23)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(154, 24)
        Me.Label44.TabIndex = 13
        Me.Label44.Text = "Módulo de caja"
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.iTotalHielo)
        Me.GroupBox13.Controls.Add(Me.lTotalCaja)
        Me.GroupBox13.Location = New System.Drawing.Point(519, 518)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(275, 78)
        Me.GroupBox13.TabIndex = 50
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "Total"
        '
        'iTotalHielo
        '
        Me.iTotalHielo.AutoSize = True
        Me.iTotalHielo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.iTotalHielo.ForeColor = System.Drawing.Color.Red
        Me.iTotalHielo.Location = New System.Drawing.Point(41, 48)
        Me.iTotalHielo.Name = "iTotalHielo"
        Me.iTotalHielo.Size = New System.Drawing.Size(0, 13)
        Me.iTotalHielo.TabIndex = 8
        '
        'lTotalCaja
        '
        Me.lTotalCaja.AutoSize = True
        Me.lTotalCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTotalCaja.ForeColor = System.Drawing.Color.Red
        Me.lTotalCaja.Location = New System.Drawing.Point(40, 22)
        Me.lTotalCaja.Name = "lTotalCaja"
        Me.lTotalCaja.Size = New System.Drawing.Size(0, 13)
        Me.lTotalCaja.TabIndex = 7
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.tbEnvasePuertaCaja)
        Me.GroupBox10.Controls.Add(Me.Label77)
        Me.GroupBox10.Controls.Add(Me.tbObsPuertaCaja)
        Me.GroupBox10.Controls.Add(Me.Label72)
        Me.GroupBox10.Controls.Add(Me.GroupBox11)
        Me.GroupBox10.Controls.Add(Me.tbLlavesPuertaCaja)
        Me.GroupBox10.Controls.Add(Me.tbHieloPuertaCaja)
        Me.GroupBox10.Controls.Add(Me.tbDestiladoPuertaCaja)
        Me.GroupBox10.Controls.Add(Me.tbLiquidoPuertaCaja)
        Me.GroupBox10.Controls.Add(Me.Label53)
        Me.GroupBox10.Controls.Add(Me.Label52)
        Me.GroupBox10.Controls.Add(Me.Label51)
        Me.GroupBox10.Controls.Add(Me.Label50)
        Me.GroupBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox10.Location = New System.Drawing.Point(210, 70)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(367, 439)
        Me.GroupBox10.TabIndex = 16
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Puerta"
        '
        'tbEnvasePuertaCaja
        '
        Me.tbEnvasePuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEnvasePuertaCaja.Location = New System.Drawing.Point(83, 158)
        Me.tbEnvasePuertaCaja.Name = "tbEnvasePuertaCaja"
        Me.tbEnvasePuertaCaja.ReadOnly = True
        Me.tbEnvasePuertaCaja.Size = New System.Drawing.Size(74, 20)
        Me.tbEnvasePuertaCaja.TabIndex = 21
        Me.tbEnvasePuertaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(18, 161)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(53, 13)
        Me.Label77.TabIndex = 1000
        Me.Label77.Text = "Envase:"
        '
        'tbObsPuertaCaja
        '
        Me.tbObsPuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbObsPuertaCaja.Location = New System.Drawing.Point(102, 366)
        Me.tbObsPuertaCaja.Multiline = True
        Me.tbObsPuertaCaja.Name = "tbObsPuertaCaja"
        Me.tbObsPuertaCaja.ReadOnly = True
        Me.tbObsPuertaCaja.Size = New System.Drawing.Size(257, 61)
        Me.tbObsPuertaCaja.TabIndex = 27
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(9, 369)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(95, 13)
        Me.Label72.TabIndex = 1000
        Me.Label72.Text = "Observaciones:"
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.tb1000PuertaCaja)
        Me.GroupBox11.Controls.Add(Me.Label62)
        Me.GroupBox11.Controls.Add(Me.tb1500PuertaCaja)
        Me.GroupBox11.Controls.Add(Me.tb500PuertaCaja)
        Me.GroupBox11.Controls.Add(Me.tb355PuertaCaja)
        Me.GroupBox11.Controls.Add(Me.Label54)
        Me.GroupBox11.Controls.Add(Me.Label55)
        Me.GroupBox11.Controls.Add(Me.Label56)
        Me.GroupBox11.Location = New System.Drawing.Point(12, 205)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(157, 144)
        Me.GroupBox11.TabIndex = 22
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Caja"
        '
        'tb1000PuertaCaja
        '
        Me.tb1000PuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb1000PuertaCaja.Location = New System.Drawing.Point(71, 78)
        Me.tb1000PuertaCaja.Name = "tb1000PuertaCaja"
        Me.tb1000PuertaCaja.ReadOnly = True
        Me.tb1000PuertaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb1000PuertaCaja.TabIndex = 25
        Me.tb1000PuertaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(13, 82)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(51, 13)
        Me.Label62.TabIndex = 1000
        Me.Label62.Text = "1000ml:"
        '
        'tb1500PuertaCaja
        '
        Me.tb1500PuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb1500PuertaCaja.Location = New System.Drawing.Point(71, 104)
        Me.tb1500PuertaCaja.Name = "tb1500PuertaCaja"
        Me.tb1500PuertaCaja.ReadOnly = True
        Me.tb1500PuertaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb1500PuertaCaja.TabIndex = 26
        Me.tb1500PuertaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tb500PuertaCaja
        '
        Me.tb500PuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb500PuertaCaja.Location = New System.Drawing.Point(71, 52)
        Me.tb500PuertaCaja.Name = "tb500PuertaCaja"
        Me.tb500PuertaCaja.ReadOnly = True
        Me.tb500PuertaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb500PuertaCaja.TabIndex = 24
        Me.tb500PuertaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tb355PuertaCaja
        '
        Me.tb355PuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb355PuertaCaja.Location = New System.Drawing.Point(71, 24)
        Me.tb355PuertaCaja.Name = "tb355PuertaCaja"
        Me.tb355PuertaCaja.ReadOnly = True
        Me.tb355PuertaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb355PuertaCaja.TabIndex = 23
        Me.tb355PuertaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(14, 108)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(51, 13)
        Me.Label54.TabIndex = 1000
        Me.Label54.Text = "1500ml:"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(20, 55)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(44, 13)
        Me.Label55.TabIndex = 1000
        Me.Label55.Text = "500ml:"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(21, 26)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(44, 13)
        Me.Label56.TabIndex = 1000
        Me.Label56.Text = "355ml:"
        '
        'tbLlavesPuertaCaja
        '
        Me.tbLlavesPuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLlavesPuertaCaja.Location = New System.Drawing.Point(82, 130)
        Me.tbLlavesPuertaCaja.Name = "tbLlavesPuertaCaja"
        Me.tbLlavesPuertaCaja.ReadOnly = True
        Me.tbLlavesPuertaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbLlavesPuertaCaja.TabIndex = 20
        Me.tbLlavesPuertaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbHieloPuertaCaja
        '
        Me.tbHieloPuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbHieloPuertaCaja.Location = New System.Drawing.Point(82, 104)
        Me.tbHieloPuertaCaja.Name = "tbHieloPuertaCaja"
        Me.tbHieloPuertaCaja.ReadOnly = True
        Me.tbHieloPuertaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbHieloPuertaCaja.TabIndex = 19
        Me.tbHieloPuertaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbDestiladoPuertaCaja
        '
        Me.tbDestiladoPuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDestiladoPuertaCaja.Location = New System.Drawing.Point(82, 78)
        Me.tbDestiladoPuertaCaja.Name = "tbDestiladoPuertaCaja"
        Me.tbDestiladoPuertaCaja.ReadOnly = True
        Me.tbDestiladoPuertaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbDestiladoPuertaCaja.TabIndex = 18
        Me.tbDestiladoPuertaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbLiquidoPuertaCaja
        '
        Me.tbLiquidoPuertaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLiquidoPuertaCaja.Location = New System.Drawing.Point(82, 52)
        Me.tbLiquidoPuertaCaja.Name = "tbLiquidoPuertaCaja"
        Me.tbLiquidoPuertaCaja.ReadOnly = True
        Me.tbLiquidoPuertaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbLiquidoPuertaCaja.TabIndex = 17
        Me.tbLiquidoPuertaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(18, 135)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(48, 13)
        Me.Label53.TabIndex = 1000
        Me.Label53.Text = "Llaves:"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(18, 109)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(40, 13)
        Me.Label52.TabIndex = 1000
        Me.Label52.Text = "Hielo:"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(18, 83)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(64, 13)
        Me.Label51.TabIndex = 1000
        Me.Label51.Text = "Destilado:"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(18, 56)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(54, 13)
        Me.Label50.TabIndex = 1000
        Me.Label50.Text = "Líquido:"
        '
        'btnGuardarCaja
        '
        Me.btnGuardarCaja.Location = New System.Drawing.Point(896, 573)
        Me.btnGuardarCaja.Name = "btnGuardarCaja"
        Me.btnGuardarCaja.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardarCaja.TabIndex = 4
        Me.btnGuardarCaja.Text = "Guardar"
        Me.btnGuardarCaja.UseVisualStyleBackColor = True
        '
        'btnLimpiarCaja
        '
        Me.btnLimpiarCaja.Location = New System.Drawing.Point(815, 573)
        Me.btnLimpiarCaja.Name = "btnLimpiarCaja"
        Me.btnLimpiarCaja.Size = New System.Drawing.Size(75, 23)
        Me.btnLimpiarCaja.TabIndex = 3
        Me.btnLimpiarCaja.Text = "Limpiar"
        Me.btnLimpiarCaja.UseVisualStyleBackColor = True
        '
        'btnBuscarCaja
        '
        Me.btnBuscarCaja.Location = New System.Drawing.Point(167, 26)
        Me.btnBuscarCaja.Name = "btnBuscarCaja"
        Me.btnBuscarCaja.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscarCaja.TabIndex = 1
        Me.btnBuscarCaja.Text = "Buscar"
        Me.btnBuscarCaja.UseVisualStyleBackColor = True
        '
        'cbCerradaCaja
        '
        Me.cbCerradaCaja.AutoSize = True
        Me.cbCerradaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCerradaCaja.Location = New System.Drawing.Point(906, 528)
        Me.cbCerradaCaja.Name = "cbCerradaCaja"
        Me.cbCerradaCaja.Size = New System.Drawing.Size(60, 17)
        Me.cbCerradaCaja.TabIndex = 2
        Me.cbCerradaCaja.Text = "Cerrar"
        Me.cbCerradaCaja.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.GroupBox12)
        Me.GroupBox5.Controls.Add(Me.Label43)
        Me.GroupBox5.Controls.Add(Me.tbObsRegresoCaja)
        Me.GroupBox5.Controls.Add(Me.GroupBox8)
        Me.GroupBox5.Controls.Add(Me.GroupBox7)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(590, 70)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(381, 439)
        Me.GroupBox5.TabIndex = 28
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Regreso"
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.tbDif1500)
        Me.GroupBox12.Controls.Add(Me.tbDif1000)
        Me.GroupBox12.Controls.Add(Me.tbDif500)
        Me.GroupBox12.Controls.Add(Me.tbDif355)
        Me.GroupBox12.Controls.Add(Me.Label64)
        Me.GroupBox12.Controls.Add(Me.Label65)
        Me.GroupBox12.Controls.Add(Me.Label66)
        Me.GroupBox12.Controls.Add(Me.Label67)
        Me.GroupBox12.Location = New System.Drawing.Point(203, 208)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(167, 142)
        Me.GroupBox12.TabIndex = 44
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Diferencia"
        '
        'tbDif1500
        '
        Me.tbDif1500.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDif1500.Location = New System.Drawing.Point(66, 101)
        Me.tbDif1500.Name = "tbDif1500"
        Me.tbDif1500.ReadOnly = True
        Me.tbDif1500.Size = New System.Drawing.Size(75, 20)
        Me.tbDif1500.TabIndex = 48
        Me.tbDif1500.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbDif1000
        '
        Me.tbDif1000.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDif1000.Location = New System.Drawing.Point(66, 75)
        Me.tbDif1000.Name = "tbDif1000"
        Me.tbDif1000.ReadOnly = True
        Me.tbDif1000.Size = New System.Drawing.Size(75, 20)
        Me.tbDif1000.TabIndex = 47
        Me.tbDif1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbDif500
        '
        Me.tbDif500.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDif500.Location = New System.Drawing.Point(66, 49)
        Me.tbDif500.Name = "tbDif500"
        Me.tbDif500.ReadOnly = True
        Me.tbDif500.Size = New System.Drawing.Size(75, 20)
        Me.tbDif500.TabIndex = 46
        Me.tbDif500.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbDif355
        '
        Me.tbDif355.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDif355.Location = New System.Drawing.Point(66, 22)
        Me.tbDif355.Name = "tbDif355"
        Me.tbDif355.ReadOnly = True
        Me.tbDif355.Size = New System.Drawing.Size(75, 20)
        Me.tbDif355.TabIndex = 45
        Me.tbDif355.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(15, 76)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(51, 13)
        Me.Label64.TabIndex = 1000
        Me.Label64.Text = "1000ml:"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(15, 104)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(51, 13)
        Me.Label65.TabIndex = 1000
        Me.Label65.Text = "1500ml:"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(21, 52)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(44, 13)
        Me.Label66.TabIndex = 1000
        Me.Label66.Text = "500ml:"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(21, 27)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(44, 13)
        Me.Label67.TabIndex = 1000
        Me.Label67.Text = "355ml:"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(12, 369)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(95, 13)
        Me.Label43.TabIndex = 1000
        Me.Label43.Text = "Observaciones:"
        '
        'tbObsRegresoCaja
        '
        Me.tbObsRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbObsRegresoCaja.Location = New System.Drawing.Point(113, 366)
        Me.tbObsRegresoCaja.Multiline = True
        Me.tbObsRegresoCaja.Name = "tbObsRegresoCaja"
        Me.tbObsRegresoCaja.ReadOnly = True
        Me.tbObsRegresoCaja.Size = New System.Drawing.Size(257, 61)
        Me.tbObsRegresoCaja.TabIndex = 49
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.tb1000RegresoCaja)
        Me.GroupBox8.Controls.Add(Me.Label63)
        Me.GroupBox8.Controls.Add(Me.tb1500RegresoCaja)
        Me.GroupBox8.Controls.Add(Me.tb500RegresoCaja)
        Me.GroupBox8.Controls.Add(Me.tb355RegresoCaja)
        Me.GroupBox8.Controls.Add(Me.Label42)
        Me.GroupBox8.Controls.Add(Me.Label41)
        Me.GroupBox8.Controls.Add(Me.Label40)
        Me.GroupBox8.Location = New System.Drawing.Point(11, 208)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(167, 142)
        Me.GroupBox8.TabIndex = 39
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Caja"
        '
        'tb1000RegresoCaja
        '
        Me.tb1000RegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb1000RegresoCaja.Location = New System.Drawing.Point(70, 77)
        Me.tb1000RegresoCaja.Name = "tb1000RegresoCaja"
        Me.tb1000RegresoCaja.ReadOnly = True
        Me.tb1000RegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb1000RegresoCaja.TabIndex = 42
        Me.tb1000RegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(20, 80)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(51, 13)
        Me.Label63.TabIndex = 1000
        Me.Label63.Text = "1000ml:"
        '
        'tb1500RegresoCaja
        '
        Me.tb1500RegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb1500RegresoCaja.Location = New System.Drawing.Point(70, 105)
        Me.tb1500RegresoCaja.Name = "tb1500RegresoCaja"
        Me.tb1500RegresoCaja.ReadOnly = True
        Me.tb1500RegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb1500RegresoCaja.TabIndex = 43
        Me.tb1500RegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tb500RegresoCaja
        '
        Me.tb500RegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb500RegresoCaja.Location = New System.Drawing.Point(70, 50)
        Me.tb500RegresoCaja.Name = "tb500RegresoCaja"
        Me.tb500RegresoCaja.ReadOnly = True
        Me.tb500RegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb500RegresoCaja.TabIndex = 41
        Me.tb500RegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tb355RegresoCaja
        '
        Me.tb355RegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb355RegresoCaja.Location = New System.Drawing.Point(70, 24)
        Me.tb355RegresoCaja.Name = "tb355RegresoCaja"
        Me.tb355RegresoCaja.ReadOnly = True
        Me.tb355RegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb355RegresoCaja.TabIndex = 40
        Me.tb355RegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(20, 108)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(51, 13)
        Me.Label42.TabIndex = 1000
        Me.Label42.Text = "1500ml:"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(23, 56)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(44, 13)
        Me.Label41.TabIndex = 1000
        Me.Label41.Text = "500ml:"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(23, 31)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(44, 13)
        Me.Label40.TabIndex = 1000
        Me.Label40.Text = "355ml:"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.tbRecuperadosRegresoCaja)
        Me.GroupBox7.Controls.Add(Me.Label57)
        Me.GroupBox7.Controls.Add(Me.tbHieloRegresoCaja)
        Me.GroupBox7.Controls.Add(Me.Label31)
        Me.GroupBox7.Controls.Add(Me.tbPrestamoRegresoCaja)
        Me.GroupBox7.Controls.Add(Me.Label45)
        Me.GroupBox7.Controls.Add(Me.tbVentRegresoCaja)
        Me.GroupBox7.Controls.Add(Me.tbLlavesRegresoCaja)
        Me.GroupBox7.Controls.Add(Me.tbLlenosRegresoCaja)
        Me.GroupBox7.Controls.Add(Me.tbCompetenciaRegresoCaja)
        Me.GroupBox7.Controls.Add(Me.tbEnvaseRegresoCaja)
        Me.GroupBox7.Controls.Add(Me.tbRotosRegresoCaja)
        Me.GroupBox7.Controls.Add(Me.Label39)
        Me.GroupBox7.Controls.Add(Me.Label38)
        Me.GroupBox7.Controls.Add(Me.Label37)
        Me.GroupBox7.Controls.Add(Me.Label36)
        Me.GroupBox7.Controls.Add(Me.Label35)
        Me.GroupBox7.Controls.Add(Me.Label34)
        Me.GroupBox7.Location = New System.Drawing.Point(11, 32)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(359, 167)
        Me.GroupBox7.TabIndex = 29
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Envases"
        '
        'tbRecuperadosRegresoCaja
        '
        Me.tbRecuperadosRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRecuperadosRegresoCaja.Location = New System.Drawing.Point(275, 111)
        Me.tbRecuperadosRegresoCaja.Name = "tbRecuperadosRegresoCaja"
        Me.tbRecuperadosRegresoCaja.ReadOnly = True
        Me.tbRecuperadosRegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbRecuperadosRegresoCaja.TabIndex = 38
        Me.tbRecuperadosRegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(189, 116)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(86, 13)
        Me.Label57.TabIndex = 1000
        Me.Label57.Text = "Recuperados:"
        '
        'tbHieloRegresoCaja
        '
        Me.tbHieloRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbHieloRegresoCaja.Location = New System.Drawing.Point(101, 79)
        Me.tbHieloRegresoCaja.Name = "tbHieloRegresoCaja"
        Me.tbHieloRegresoCaja.ReadOnly = True
        Me.tbHieloRegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbHieloRegresoCaja.TabIndex = 32
        Me.tbHieloRegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(11, 81)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(40, 13)
        Me.Label31.TabIndex = 1000
        Me.Label31.Text = "Hielo:"
        '
        'tbPrestamoRegresoCaja
        '
        Me.tbPrestamoRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPrestamoRegresoCaja.Location = New System.Drawing.Point(274, 54)
        Me.tbPrestamoRegresoCaja.Name = "tbPrestamoRegresoCaja"
        Me.tbPrestamoRegresoCaja.ReadOnly = True
        Me.tbPrestamoRegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbPrestamoRegresoCaja.TabIndex = 36
        Me.tbPrestamoRegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(188, 59)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(63, 13)
        Me.Label45.TabIndex = 1000
        Me.Label45.Text = "Prestamo:"
        '
        'tbVentRegresoCaja
        '
        Me.tbVentRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbVentRegresoCaja.Location = New System.Drawing.Point(101, 24)
        Me.tbVentRegresoCaja.Name = "tbVentRegresoCaja"
        Me.tbVentRegresoCaja.ReadOnly = True
        Me.tbVentRegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbVentRegresoCaja.TabIndex = 30
        Me.tbVentRegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbLlavesRegresoCaja
        '
        Me.tbLlavesRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLlavesRegresoCaja.Location = New System.Drawing.Point(101, 105)
        Me.tbLlavesRegresoCaja.Name = "tbLlavesRegresoCaja"
        Me.tbLlavesRegresoCaja.ReadOnly = True
        Me.tbLlavesRegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbLlavesRegresoCaja.TabIndex = 33
        Me.tbLlavesRegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbLlenosRegresoCaja
        '
        Me.tbLlenosRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLlenosRegresoCaja.Location = New System.Drawing.Point(101, 133)
        Me.tbLlenosRegresoCaja.Name = "tbLlenosRegresoCaja"
        Me.tbLlenosRegresoCaja.ReadOnly = True
        Me.tbLlenosRegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbLlenosRegresoCaja.TabIndex = 34
        Me.tbLlenosRegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbCompetenciaRegresoCaja
        '
        Me.tbCompetenciaRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCompetenciaRegresoCaja.Location = New System.Drawing.Point(275, 83)
        Me.tbCompetenciaRegresoCaja.Name = "tbCompetenciaRegresoCaja"
        Me.tbCompetenciaRegresoCaja.ReadOnly = True
        Me.tbCompetenciaRegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbCompetenciaRegresoCaja.TabIndex = 37
        Me.tbCompetenciaRegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbEnvaseRegresoCaja
        '
        Me.tbEnvaseRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEnvaseRegresoCaja.Location = New System.Drawing.Point(101, 53)
        Me.tbEnvaseRegresoCaja.Name = "tbEnvaseRegresoCaja"
        Me.tbEnvaseRegresoCaja.ReadOnly = True
        Me.tbEnvaseRegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbEnvaseRegresoCaja.TabIndex = 31
        Me.tbEnvaseRegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbRotosRegresoCaja
        '
        Me.tbRotosRegresoCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRotosRegresoCaja.Location = New System.Drawing.Point(274, 24)
        Me.tbRotosRegresoCaja.Name = "tbRotosRegresoCaja"
        Me.tbRotosRegresoCaja.ReadOnly = True
        Me.tbRotosRegresoCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbRotosRegresoCaja.TabIndex = 35
        Me.tbRotosRegresoCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(11, 24)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(89, 13)
        Me.Label39.TabIndex = 1000
        Me.Label39.Text = "Total de viaje:"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(12, 108)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(48, 13)
        Me.Label38.TabIndex = 1000
        Me.Label38.Text = "Llaves:"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(14, 140)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(48, 13)
        Me.Label37.TabIndex = 1000
        Me.Label37.Text = "Llenos:"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(191, 85)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(84, 13)
        Me.Label36.TabIndex = 1000
        Me.Label36.Text = "Competencia:"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(10, 56)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(53, 13)
        Me.Label35.TabIndex = 1000
        Me.Label35.Text = "Envase:"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(191, 27)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(44, 13)
        Me.Label34.TabIndex = 1000
        Me.Label34.Text = "Rotos:"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.tbEnvaseCargaCaja)
        Me.GroupBox4.Controls.Add(Me.Label76)
        Me.GroupBox4.Controls.Add(Me.GroupBox6)
        Me.GroupBox4.Controls.Add(Me.tbHieloCargaCaja)
        Me.GroupBox4.Controls.Add(Me.tbDestiladoCargaCaja)
        Me.GroupBox4.Controls.Add(Me.tbLiquidoCargaCaja)
        Me.GroupBox4.Controls.Add(Me.Label30)
        Me.GroupBox4.Controls.Add(Me.Label29)
        Me.GroupBox4.Controls.Add(Me.Label28)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(18, 70)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(179, 439)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Carga"
        '
        'tbEnvaseCargaCaja
        '
        Me.tbEnvaseCargaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEnvaseCargaCaja.Location = New System.Drawing.Point(82, 159)
        Me.tbEnvaseCargaCaja.Name = "tbEnvaseCargaCaja"
        Me.tbEnvaseCargaCaja.ReadOnly = True
        Me.tbEnvaseCargaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbEnvaseCargaCaja.TabIndex = 10
        Me.tbEnvaseCargaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(17, 161)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(53, 13)
        Me.Label76.TabIndex = 1000
        Me.Label76.Text = "Envase:"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.tb1000CargaCaja)
        Me.GroupBox6.Controls.Add(Me.Label60)
        Me.GroupBox6.Controls.Add(Me.tb1500CargaCaja)
        Me.GroupBox6.Controls.Add(Me.tb500CargaCaja)
        Me.GroupBox6.Controls.Add(Me.tb355CargaCaja)
        Me.GroupBox6.Controls.Add(Me.Label33)
        Me.GroupBox6.Controls.Add(Me.Label32)
        Me.GroupBox6.Controls.Add(Me.Label5)
        Me.GroupBox6.Location = New System.Drawing.Point(11, 202)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(157, 147)
        Me.GroupBox6.TabIndex = 11
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Caja"
        '
        'tb1000CargaCaja
        '
        Me.tb1000CargaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb1000CargaCaja.Location = New System.Drawing.Point(71, 78)
        Me.tb1000CargaCaja.Name = "tb1000CargaCaja"
        Me.tb1000CargaCaja.ReadOnly = True
        Me.tb1000CargaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb1000CargaCaja.TabIndex = 14
        Me.tb1000CargaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(12, 80)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(51, 13)
        Me.Label60.TabIndex = 1000
        Me.Label60.Text = "1000ml:"
        '
        'tb1500CargaCaja
        '
        Me.tb1500CargaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb1500CargaCaja.Location = New System.Drawing.Point(71, 104)
        Me.tb1500CargaCaja.Name = "tb1500CargaCaja"
        Me.tb1500CargaCaja.ReadOnly = True
        Me.tb1500CargaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb1500CargaCaja.TabIndex = 15
        Me.tb1500CargaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tb500CargaCaja
        '
        Me.tb500CargaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb500CargaCaja.Location = New System.Drawing.Point(71, 51)
        Me.tb500CargaCaja.Name = "tb500CargaCaja"
        Me.tb500CargaCaja.ReadOnly = True
        Me.tb500CargaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb500CargaCaja.TabIndex = 13
        Me.tb500CargaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tb355CargaCaja
        '
        Me.tb355CargaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb355CargaCaja.Location = New System.Drawing.Point(71, 22)
        Me.tb355CargaCaja.Name = "tb355CargaCaja"
        Me.tb355CargaCaja.ReadOnly = True
        Me.tb355CargaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tb355CargaCaja.TabIndex = 12
        Me.tb355CargaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(12, 107)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(51, 13)
        Me.Label33.TabIndex = 1000
        Me.Label33.Text = "1500ml:"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(18, 55)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(44, 13)
        Me.Label32.TabIndex = 1000
        Me.Label32.Text = "500ml:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(19, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 1000
        Me.Label5.Text = "355ml:"
        '
        'tbHieloCargaCaja
        '
        Me.tbHieloCargaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbHieloCargaCaja.Location = New System.Drawing.Point(82, 104)
        Me.tbHieloCargaCaja.Name = "tbHieloCargaCaja"
        Me.tbHieloCargaCaja.ReadOnly = True
        Me.tbHieloCargaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbHieloCargaCaja.TabIndex = 9
        Me.tbHieloCargaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbDestiladoCargaCaja
        '
        Me.tbDestiladoCargaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDestiladoCargaCaja.Location = New System.Drawing.Point(82, 78)
        Me.tbDestiladoCargaCaja.Name = "tbDestiladoCargaCaja"
        Me.tbDestiladoCargaCaja.ReadOnly = True
        Me.tbDestiladoCargaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbDestiladoCargaCaja.TabIndex = 8
        Me.tbDestiladoCargaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbLiquidoCargaCaja
        '
        Me.tbLiquidoCargaCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLiquidoCargaCaja.Location = New System.Drawing.Point(82, 49)
        Me.tbLiquidoCargaCaja.Name = "tbLiquidoCargaCaja"
        Me.tbLiquidoCargaCaja.ReadOnly = True
        Me.tbLiquidoCargaCaja.Size = New System.Drawing.Size(75, 20)
        Me.tbLiquidoCargaCaja.TabIndex = 7
        Me.tbLiquidoCargaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(17, 107)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(40, 13)
        Me.Label30.TabIndex = 1000
        Me.Label30.Text = "Hielo:"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 81)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(64, 13)
        Me.Label29.TabIndex = 1000
        Me.Label29.Text = "Destilado:"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(17, 53)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(54, 13)
        Me.Label28.TabIndex = 1000
        Me.Label28.Text = "Líquido:"
        '
        'tbIdCaja
        '
        Me.tbIdCaja.Location = New System.Drawing.Point(49, 28)
        Me.tbIdCaja.Name = "tbIdCaja"
        Me.tbIdCaja.Size = New System.Drawing.Size(100, 20)
        Me.tbIdCaja.TabIndex = 0
        Me.tbIdCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(24, 31)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(22, 13)
        Me.Label27.TabIndex = 1000
        Me.Label27.Text = "Id:"
        '
        'cargaPanel
        '
        Me.cargaPanel.Controls.Add(Me.tbEnvaseCarga)
        Me.cargaPanel.Controls.Add(Me.Label75)
        Me.cargaPanel.Controls.Add(Me.tbRutaCarga)
        Me.cargaPanel.Controls.Add(Me.Label69)
        Me.cargaPanel.Controls.Add(Me.GroupBox9)
        Me.cargaPanel.Controls.Add(Me.tbHieloCarga)
        Me.cargaPanel.Controls.Add(Me.Label46)
        Me.cargaPanel.Controls.Add(Me.tbDestiladoCarga)
        Me.cargaPanel.Controls.Add(Me.Label47)
        Me.cargaPanel.Controls.Add(Me.tbLiquidoCarga)
        Me.cargaPanel.Controls.Add(Me.Label48)
        Me.cargaPanel.Controls.Add(Me.fechaLabel)
        Me.cargaPanel.Controls.Add(Me.cantidadLabel)
        Me.cargaPanel.Controls.Add(Me.rutaLabel)
        Me.cargaPanel.Controls.Add(Me.btnLimpiarCarga)
        Me.cargaPanel.Controls.Add(Me.idLabel)
        Me.cargaPanel.Controls.Add(Me.tbIdCarga)
        Me.cargaPanel.Controls.Add(Me.tbCantidadCarga)
        Me.cargaPanel.Controls.Add(Me.tbFechaCarga)
        Me.cargaPanel.Controls.Add(Me.btnGuardarCarga)
        Me.cargaPanel.Controls.Add(Me.btnBuscarCarga)
        Me.cargaPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cargaPanel.Location = New System.Drawing.Point(0, 24)
        Me.cargaPanel.Name = "cargaPanel"
        Me.cargaPanel.Size = New System.Drawing.Size(996, 613)
        Me.cargaPanel.TabIndex = 16
        Me.cargaPanel.Visible = False
        '
        'tbEnvaseCarga
        '
        Me.tbEnvaseCarga.Location = New System.Drawing.Point(458, 286)
        Me.tbEnvaseCarga.Name = "tbEnvaseCarga"
        Me.tbEnvaseCarga.Size = New System.Drawing.Size(100, 20)
        Me.tbEnvaseCarga.TabIndex = 5
        Me.tbEnvaseCarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.Location = New System.Drawing.Point(391, 292)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(53, 13)
        Me.Label75.TabIndex = 1000
        Me.Label75.Text = "Envase:"
        '
        'tbRutaCarga
        '
        Me.tbRutaCarga.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.tbRutaCarga.Location = New System.Drawing.Point(262, 232)
        Me.tbRutaCarga.Name = "tbRutaCarga"
        Me.tbRutaCarga.Size = New System.Drawing.Size(114, 21)
        Me.tbRutaCarga.TabIndex = 2
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label69.Location = New System.Drawing.Point(805, 28)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(168, 24)
        Me.Label69.TabIndex = 39
        Me.Label69.Text = "Módulo de carga"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.tbCaja1500Carga)
        Me.GroupBox9.Controls.Add(Me.Label58)
        Me.GroupBox9.Controls.Add(Me.tbCaja1000Carga)
        Me.GroupBox9.Controls.Add(Me.tbCaja500Carga)
        Me.GroupBox9.Controls.Add(Me.Label1)
        Me.GroupBox9.Controls.Add(Me.Label2)
        Me.GroupBox9.Controls.Add(Me.tbCaja355Carga)
        Me.GroupBox9.Controls.Add(Me.Label3)
        Me.GroupBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox9.Location = New System.Drawing.Point(565, 213)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(179, 138)
        Me.GroupBox9.TabIndex = 8
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Caja"
        '
        'tbCaja1500Carga
        '
        Me.tbCaja1500Carga.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja1500Carga.Location = New System.Drawing.Point(65, 100)
        Me.tbCaja1500Carga.Name = "tbCaja1500Carga"
        Me.tbCaja1500Carga.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja1500Carga.TabIndex = 11
        Me.tbCaja1500Carga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(8, 106)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(51, 13)
        Me.Label58.TabIndex = 1000
        Me.Label58.Text = "1500ml:"
        '
        'tbCaja1000Carga
        '
        Me.tbCaja1000Carga.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja1000Carga.Location = New System.Drawing.Point(65, 74)
        Me.tbCaja1000Carga.Name = "tbCaja1000Carga"
        Me.tbCaja1000Carga.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja1000Carga.TabIndex = 10
        Me.tbCaja1000Carga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tbCaja500Carga
        '
        Me.tbCaja500Carga.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja500Carga.Location = New System.Drawing.Point(65, 48)
        Me.tbCaja500Carga.Name = "tbCaja500Carga"
        Me.tbCaja500Carga.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja500Carga.TabIndex = 9
        Me.tbCaja500Carga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 77)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 13)
        Me.Label1.TabIndex = 1000
        Me.Label1.Text = "1000ml:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 1000
        Me.Label2.Text = "500ml:"
        '
        'tbCaja355Carga
        '
        Me.tbCaja355Carga.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCaja355Carga.Location = New System.Drawing.Point(65, 23)
        Me.tbCaja355Carga.Name = "tbCaja355Carga"
        Me.tbCaja355Carga.Size = New System.Drawing.Size(100, 20)
        Me.tbCaja355Carga.TabIndex = 8
        Me.tbCaja355Carga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 1000
        Me.Label3.Text = "355ml:"
        '
        'tbHieloCarga
        '
        Me.tbHieloCarga.Location = New System.Drawing.Point(458, 312)
        Me.tbHieloCarga.Name = "tbHieloCarga"
        Me.tbHieloCarga.Size = New System.Drawing.Size(100, 20)
        Me.tbHieloCarga.TabIndex = 6
        Me.tbHieloCarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(391, 315)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(40, 13)
        Me.Label46.TabIndex = 1000
        Me.Label46.Text = "Hielo:"
        '
        'tbDestiladoCarga
        '
        Me.tbDestiladoCarga.Location = New System.Drawing.Point(458, 261)
        Me.tbDestiladoCarga.Name = "tbDestiladoCarga"
        Me.tbDestiladoCarga.Size = New System.Drawing.Size(100, 20)
        Me.tbDestiladoCarga.TabIndex = 4
        Me.tbDestiladoCarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(389, 265)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(64, 13)
        Me.Label47.TabIndex = 1000
        Me.Label47.Text = "Destilado:"
        '
        'tbLiquidoCarga
        '
        Me.tbLiquidoCarga.Location = New System.Drawing.Point(458, 235)
        Me.tbLiquidoCarga.Name = "tbLiquidoCarga"
        Me.tbLiquidoCarga.Size = New System.Drawing.Size(100, 20)
        Me.tbLiquidoCarga.TabIndex = 3
        Me.tbLiquidoCarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(390, 238)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(54, 13)
        Me.Label48.TabIndex = 1000
        Me.Label48.Text = "Líquido:"
        '
        'rutaPanel
        '
        Me.rutaPanel.Controls.Add(Me.GroupBox15)
        Me.rutaPanel.Controls.Add(Me.GroupBox14)
        Me.rutaPanel.Controls.Add(Me.Label74)
        Me.rutaPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rutaPanel.Location = New System.Drawing.Point(0, 24)
        Me.rutaPanel.Name = "rutaPanel"
        Me.rutaPanel.Size = New System.Drawing.Size(996, 613)
        Me.rutaPanel.TabIndex = 27
        Me.rutaPanel.Visible = False
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.lbRuta)
        Me.GroupBox15.Controls.Add(Me.Button4)
        Me.GroupBox15.Controls.Add(Me.Button6)
        Me.GroupBox15.Controls.Add(Me.Button5)
        Me.GroupBox15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox15.Location = New System.Drawing.Point(181, 81)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(310, 371)
        Me.GroupBox15.TabIndex = 47
        Me.GroupBox15.TabStop = False
        Me.GroupBox15.Text = "Orden"
        '
        'lbRuta
        '
        Me.lbRuta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbRuta.FormattingEnabled = True
        Me.lbRuta.Location = New System.Drawing.Point(22, 34)
        Me.lbRuta.Name = "lbRuta"
        Me.lbRuta.Size = New System.Drawing.Size(183, 277)
        Me.lbRuta.TabIndex = 19
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(214, 135)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 20
        Me.Button4.Text = "Arriba"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(211, 326)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 22
        Me.Button6.Text = "Guardar"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(214, 164)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 21
        Me.Button5.Text = "Abajo"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.cbRutas)
        Me.GroupBox14.Controls.Add(Me.tbRutaCatalogo)
        Me.GroupBox14.Controls.Add(Me.Button1)
        Me.GroupBox14.Controls.Add(Me.Button2)
        Me.GroupBox14.Controls.Add(Me.Button3)
        Me.GroupBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox14.Location = New System.Drawing.Point(543, 162)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(284, 174)
        Me.GroupBox14.TabIndex = 46
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "Rutas"
        '
        'cbRutas
        '
        Me.cbRutas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbRutas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbRutas.FormattingEnabled = True
        Me.cbRutas.ItemHeight = 13
        Me.cbRutas.Location = New System.Drawing.Point(33, 47)
        Me.cbRutas.Name = "cbRutas"
        Me.cbRutas.Size = New System.Drawing.Size(121, 21)
        Me.cbRutas.TabIndex = 14
        '
        'tbRutaCatalogo
        '
        Me.tbRutaCatalogo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbRutaCatalogo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRutaCatalogo.Location = New System.Drawing.Point(33, 78)
        Me.tbRutaCatalogo.Name = "tbRutaCatalogo"
        Me.tbRutaCatalogo.Size = New System.Drawing.Size(119, 20)
        Me.tbRutaCatalogo.TabIndex = 15
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(33, 127)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "Agregar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(114, 127)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 17
        Me.Button2.Text = "Actualizar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(195, 128)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 18
        Me.Button3.Text = "Borrar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label74.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label74.Location = New System.Drawing.Point(792, 28)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(173, 24)
        Me.Label74.TabIndex = 40
        Me.Label74.Text = "Catálogo de rutas"
        '
        'loginPanel
        '
        Me.loginPanel.Controls.Add(Me.GroupBox16)
        Me.loginPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.loginPanel.Location = New System.Drawing.Point(0, 24)
        Me.loginPanel.Name = "loginPanel"
        Me.loginPanel.Size = New System.Drawing.Size(996, 613)
        Me.loginPanel.TabIndex = 14
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.Button7)
        Me.GroupBox16.Controls.Add(Me.tbLoginPass)
        Me.GroupBox16.Controls.Add(Me.Label79)
        Me.GroupBox16.Controls.Add(Me.tbLoginUsr)
        Me.GroupBox16.Controls.Add(Me.Label78)
        Me.GroupBox16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox16.Location = New System.Drawing.Point(339, 213)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(333, 142)
        Me.GroupBox16.TabIndex = 0
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "Login"
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(235, 106)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 3
        Me.Button7.Text = "Acceder"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'tbLoginPass
        '
        Me.tbLoginPass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLoginPass.Location = New System.Drawing.Point(120, 65)
        Me.tbLoginPass.Name = "tbLoginPass"
        Me.tbLoginPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbLoginPass.Size = New System.Drawing.Size(100, 20)
        Me.tbLoginPass.TabIndex = 2
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.Location = New System.Drawing.Point(34, 72)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(75, 13)
        Me.Label79.TabIndex = 0
        Me.Label79.Text = "Contraseña:"
        '
        'tbLoginUsr
        '
        Me.tbLoginUsr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLoginUsr.Location = New System.Drawing.Point(119, 39)
        Me.tbLoginUsr.Name = "tbLoginUsr"
        Me.tbLoginUsr.Size = New System.Drawing.Size(100, 20)
        Me.tbLoginUsr.TabIndex = 1
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label78.Location = New System.Drawing.Point(34, 42)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(54, 13)
        Me.Label78.TabIndex = 0
        Me.Label78.Text = "Usuario:"
        '
        'usuarioPanel
        '
        Me.usuarioPanel.Controls.Add(Me.Detalle)
        Me.usuarioPanel.Controls.Add(Me.Label80)
        Me.usuarioPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.usuarioPanel.Location = New System.Drawing.Point(0, 24)
        Me.usuarioPanel.Name = "usuarioPanel"
        Me.usuarioPanel.Size = New System.Drawing.Size(996, 613)
        Me.usuarioPanel.TabIndex = 29
        Me.usuarioPanel.Visible = False
        '
        'Detalle
        '
        Me.Detalle.Controls.Add(Me.tbUserName)
        Me.Detalle.Controls.Add(Me.Label83)
        Me.Detalle.Controls.Add(Me.cbUsuarios)
        Me.Detalle.Controls.Add(Me.Button10)
        Me.Detalle.Controls.Add(Me.Button9)
        Me.Detalle.Controls.Add(Me.Button8)
        Me.Detalle.Controls.Add(Me.tbSecPassword)
        Me.Detalle.Controls.Add(Me.chkbCaja)
        Me.Detalle.Controls.Add(Me.chkbRegreso)
        Me.Detalle.Controls.Add(Me.chkbPuerta)
        Me.Detalle.Controls.Add(Me.chkbCarga)
        Me.Detalle.Controls.Add(Me.Label82)
        Me.Detalle.Controls.Add(Me.Label81)
        Me.Detalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Detalle.Location = New System.Drawing.Point(299, 180)
        Me.Detalle.Name = "Detalle"
        Me.Detalle.Size = New System.Drawing.Size(392, 248)
        Me.Detalle.TabIndex = 44
        Me.Detalle.TabStop = False
        Me.Detalle.Text = "Información del usuario"
        '
        'tbUserName
        '
        Me.tbUserName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUserName.Location = New System.Drawing.Point(119, 71)
        Me.tbUserName.Name = "tbUserName"
        Me.tbUserName.Size = New System.Drawing.Size(126, 20)
        Me.tbUserName.TabIndex = 5
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(31, 71)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(54, 13)
        Me.Label83.TabIndex = 12
        Me.Label83.Text = "Usuario:"
        '
        'cbUsuarios
        '
        Me.cbUsuarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbUsuarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbUsuarios.FormattingEnabled = True
        Me.cbUsuarios.Location = New System.Drawing.Point(119, 36)
        Me.cbUsuarios.Name = "cbUsuarios"
        Me.cbUsuarios.Size = New System.Drawing.Size(126, 21)
        Me.cbUsuarios.TabIndex = 4
        '
        'Button10
        '
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(205, 212)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 12
        Me.Button10.Text = "Limpiar"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.Location = New System.Drawing.Point(298, 212)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 23)
        Me.Button9.TabIndex = 13
        Me.Button9.Text = "Borrar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(124, 212)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 23)
        Me.Button8.TabIndex = 11
        Me.Button8.Text = "Guardar"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'tbSecPassword
        '
        Me.tbSecPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSecPassword.Location = New System.Drawing.Point(120, 100)
        Me.tbSecPassword.Name = "tbSecPassword"
        Me.tbSecPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbSecPassword.Size = New System.Drawing.Size(125, 20)
        Me.tbSecPassword.TabIndex = 6
        '
        'chkbCaja
        '
        Me.chkbCaja.AutoSize = True
        Me.chkbCaja.Location = New System.Drawing.Point(277, 151)
        Me.chkbCaja.Name = "chkbCaja"
        Me.chkbCaja.Size = New System.Drawing.Size(51, 17)
        Me.chkbCaja.TabIndex = 10
        Me.chkbCaja.Text = "Caja"
        Me.chkbCaja.UseVisualStyleBackColor = True
        '
        'chkbRegreso
        '
        Me.chkbRegreso.AutoSize = True
        Me.chkbRegreso.Location = New System.Drawing.Point(191, 151)
        Me.chkbRegreso.Name = "chkbRegreso"
        Me.chkbRegreso.Size = New System.Drawing.Size(73, 17)
        Me.chkbRegreso.TabIndex = 9
        Me.chkbRegreso.Text = "Regreso"
        Me.chkbRegreso.UseVisualStyleBackColor = True
        '
        'chkbPuerta
        '
        Me.chkbPuerta.AutoSize = True
        Me.chkbPuerta.Location = New System.Drawing.Point(111, 152)
        Me.chkbPuerta.Name = "chkbPuerta"
        Me.chkbPuerta.Size = New System.Drawing.Size(63, 17)
        Me.chkbPuerta.TabIndex = 8
        Me.chkbPuerta.Text = "Puerta"
        Me.chkbPuerta.UseVisualStyleBackColor = True
        '
        'chkbCarga
        '
        Me.chkbCarga.AutoSize = True
        Me.chkbCarga.Location = New System.Drawing.Point(34, 151)
        Me.chkbCarga.Name = "chkbCarga"
        Me.chkbCarga.Size = New System.Drawing.Size(59, 17)
        Me.chkbCarga.TabIndex = 7
        Me.chkbCarga.Text = "Carga"
        Me.chkbCarga.UseVisualStyleBackColor = True
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(31, 103)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(75, 13)
        Me.Label82.TabIndex = 1
        Me.Label82.Text = "Contraseña:"
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label81.Location = New System.Drawing.Point(31, 37)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(60, 13)
        Me.Label81.TabIndex = 0
        Me.Label81.Text = "Usuarios:"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label80.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label80.Location = New System.Drawing.Point(766, 25)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(207, 24)
        Me.Label80.TabIndex = 41
        Me.Label80.Text = "Catálogo de usuarios"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(996, 637)
        Me.Controls.Add(Me.cajaPanel)
        Me.Controls.Add(Me.puertaPanel)
        Me.Controls.Add(Me.regresoPanel)
        Me.Controls.Add(Me.cargaPanel)
        Me.Controls.Add(Me.loginPanel)
        Me.Controls.Add(Me.rutaPanel)
        Me.Controls.Add(Me.usuarioPanel)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Super Agua"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.puertaPanel.ResumeLayout(False)
        Me.puertaPanel.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.regresoPanel.ResumeLayout(False)
        Me.regresoPanel.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.cajaPanel.ResumeLayout(False)
        Me.cajaPanel.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.cargaPanel.ResumeLayout(False)
        Me.cargaPanel.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.rutaPanel.ResumeLayout(False)
        Me.rutaPanel.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.loginPanel.ResumeLayout(False)
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        Me.usuarioPanel.ResumeLayout(False)
        Me.usuarioPanel.PerformLayout()
        Me.Detalle.ResumeLayout(False)
        Me.Detalle.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModulosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuertaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegresoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AcercaDeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnBuscarCarga As System.Windows.Forms.Button
    Friend WithEvents btnGuardarCarga As System.Windows.Forms.Button
    Friend WithEvents tbFechaCarga As System.Windows.Forms.TextBox
    Friend WithEvents fechaLabel As System.Windows.Forms.Label
    Friend WithEvents tbCantidadCarga As System.Windows.Forms.TextBox
    Friend WithEvents cantidadLabel As System.Windows.Forms.Label
    Friend WithEvents tbIdCarga As System.Windows.Forms.TextBox
    Friend WithEvents idLabel As System.Windows.Forms.Label
    Friend WithEvents puertaPanel As System.Windows.Forms.Panel
    Friend WithEvents tbDestiladoPuerta As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbLiquidoPuerta As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbHieloPuerta As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnBuscarPuerta As System.Windows.Forms.Button
    Friend WithEvents btnGuardarPuerta As System.Windows.Forms.Button
    Friend WithEvents regresoPanel As System.Windows.Forms.Panel
    Friend WithEvents tbObsRegreso As System.Windows.Forms.TextBox
    Friend WithEvents tbIdRegreso As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnBuscarRegreso As System.Windows.Forms.Button
    Friend WithEvents btnGuardarRegreso As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tbLlenosRegreso As System.Windows.Forms.TextBox
    Friend WithEvents tbCompetenciaRegreso As System.Windows.Forms.TextBox
    Friend WithEvents tbEnvaseRegreso As System.Windows.Forms.TextBox
    Friend WithEvents tbRotosRegreso As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents rutaLabel As System.Windows.Forms.Label
    Friend WithEvents btnLimpiarRegreso As System.Windows.Forms.Button
    Friend WithEvents btnLimpiarPuerta As System.Windows.Forms.Button
    Friend WithEvents btnLimpiarCarga As System.Windows.Forms.Button
    Friend WithEvents HerramientasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguraciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tbLlavesPuerta As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents tbVentaRegreso As System.Windows.Forms.TextBox
    Friend WithEvents tbLlavesRegreso As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents CajaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents tbCaja1500Puerta As System.Windows.Forms.TextBox
    Friend WithEvents tbCaja500Puerta As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents tbCaja355Puerta As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents tbIdPuerta As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents tbCaja355Regreso As System.Windows.Forms.TextBox
    Friend WithEvents tbCaja1500Regreso As System.Windows.Forms.TextBox
    Friend WithEvents tbCaja500Regreso As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cajaPanel As System.Windows.Forms.Panel
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents tbIdCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents tbHieloCargaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbDestiladoCargaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbLiquidoCargaCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cbCerradaCaja As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents tb1500CargaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tb500CargaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tb355CargaCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents tbLlenosRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbCompetenciaRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbEnvaseRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbRotosRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents tbVentRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbLlavesRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents tb1500RegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents tb500RegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents tb355RegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardarCaja As System.Windows.Forms.Button
    Friend WithEvents btnLimpiarCaja As System.Windows.Forms.Button
    Friend WithEvents btnBuscarCaja As System.Windows.Forms.Button
    Friend WithEvents lTotalCaja As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents tbObsRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbPrestamoRegreso As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbPrestamoRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents cargaPanel As System.Windows.Forms.Panel
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents tbCaja1000Carga As System.Windows.Forms.TextBox
    Friend WithEvents tbCaja500Carga As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbCaja355Carga As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbHieloCarga As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents tbDestiladoCarga As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents tbLiquidoCarga As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents tbHieloRegreso As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents tbRecuperadosRegreso As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents tbLlavesPuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbHieloPuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbDestiladoPuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tbLiquidoPuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tb1500PuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tb500PuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tb355PuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents tbHieloRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents tbRecuperadosRegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents tbCaja1500Carga As System.Windows.Forms.TextBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents tbCaja1000Puerta As System.Windows.Forms.TextBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents tbCaja1000Regreso As System.Windows.Forms.TextBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents tb1000CargaCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents tb1000PuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents tb1000RegresoCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents tbDif1500 As System.Windows.Forms.TextBox
    Friend WithEvents tbDif1000 As System.Windows.Forms.TextBox
    Friend WithEvents tbDif500 As System.Windows.Forms.TextBox
    Friend WithEvents tbDif355 As System.Windows.Forms.TextBox
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents iTotalHielo As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents tbObsPuerta As System.Windows.Forms.TextBox
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents tbObsPuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents CatalogosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RutasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tbRutaCarga As System.Windows.Forms.ComboBox
    Friend WithEvents tbRutaPuerta As System.Windows.Forms.ComboBox
    Friend WithEvents rutaPanel As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tbRutaCatalogo As System.Windows.Forms.TextBox
    Friend WithEvents cbRutas As System.Windows.Forms.ComboBox
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents tbEnvaseCarga As System.Windows.Forms.TextBox
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents tbEnvaseCargaCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents lbRuta As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents tbEnvasePuerta As System.Windows.Forms.TextBox
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents tbEnvasePuertaCaja As System.Windows.Forms.TextBox
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents loginPanel As System.Windows.Forms.Panel
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents tbLoginPass As System.Windows.Forms.TextBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents tbLoginUsr As System.Windows.Forms.TextBox
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents usuarioPanel As System.Windows.Forms.Panel
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents Detalle As System.Windows.Forms.GroupBox
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents tbSecPassword As System.Windows.Forms.TextBox
    Friend WithEvents chkbCaja As System.Windows.Forms.CheckBox
    Friend WithEvents chkbRegreso As System.Windows.Forms.CheckBox
    Friend WithEvents chkbPuerta As System.Windows.Forms.CheckBox
    Friend WithEvents chkbCarga As System.Windows.Forms.CheckBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents cbUsuarios As System.Windows.Forms.ComboBox
    Friend WithEvents tbUserName As System.Windows.Forms.TextBox
    Friend WithEvents Label83 As System.Windows.Forms.Label

End Class
