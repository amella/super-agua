﻿Imports System.Windows.Forms

Public Class Preferences

    Shared Function validar()

        Dim _strRuta As String = My.Settings.RUTA

        Return Not (_strRuta = String.Empty)

    End Function

    Private Sub loadCustConnectionSettings()

        Me.tbRutaFile.Text = My.Settings.RUTA

    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        If validar() = False Then
            MessageBox.Show("Asegurese de haber ingresado los campos requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else

            My.Settings.RUTA = Me.tbRutaFile.Text

            My.Settings.Save()

            Me.Close()

        End If

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim fileDlg As New OpenFileDialog()
        fileDlg.Reset()
        fileDlg.DefaultExt = ".accdb"
        fileDlg.Filter = "Archivos de access|*.accdb"
        fileDlg.Multiselect = False

        If (fileDlg.ShowDialog() = DialogResult.OK) Then
            Me.tbRutaFile.Text = fileDlg.FileName
            My.Settings.RUTA = fileDlg.FileName
        End If
    End Sub

    Private Sub Preferences_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.loadCustConnectionSettings()
    End Sub
End Class
