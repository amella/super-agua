﻿Public Class Carga

    Public _id As Long
    Public _ruta As String
    Public _cantidad As Long
    Public _fecha As Date
    Public _cerrada As Long

    Public _app As Long
    Public _apla As Long
    Public _hielo As Long
    Public _envase As Long
    Public _caja355 As Long
    Public _caja500 As Long
    Public _caja1500 As Long
    Public _caja1000 As Long

    Public Sub New()
        Me._cerrada = 0
    End Sub

    Public Function getCantidad()
        Return _app + _apla
    End Function

End Class
