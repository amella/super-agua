﻿Imports System.Data.OleDb

Public Class SuperAguaDAO

    Public _connectionManager As ConnectionManager

    Private Function getUsuariosQuery()
        Dim foo As String
        foo = "select id, usr, pass, rols from Usuario order by usr desc"
        Return foo
    End Function

    Private Function getUsuarioByUsernameQuery(ByRef username As String)
        Dim foo As String
        foo = "select id, usr, pass, rols from Usuario where usr='" & username & "'"
        Return foo
    End Function

    Public Function getUsuarioByUsername(ByRef username As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim usuario As Usuario = New Usuario()

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getUsuarioByUsernameQuery(username), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                usuario._id = reader(0)
                usuario._username = reader(1)
                usuario._password = reader(2)
                usuario._roles = reader(3)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return usuario

    End Function

    Public Function getUsuarios()

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim usuarios As List(Of Usuario) = New List(Of Usuario)
        Dim usuario As Usuario = Nothing

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getUsuariosQuery(), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                usuario = New Usuario()
                usuario._id = reader(0)
                usuario._username = reader(1)
                usuario._password = reader(2)
                usuario._roles = reader(3)

                usuarios.Add(usuario)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return usuarios

    End Function

    Private Function existsUsuarioQuery(ByRef username As String)
        Dim foo As String
        foo = "select count(id) from Usuario where usr='" & username & "'"
        Return foo
    End Function

    Public Function existsUsuario(ByRef username As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.existsUsuarioQuery(username), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                existe = (reader(0) <> 0)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return existe

    End Function

    Private Function insertUsuarioQuery(ByRef user As Usuario)      
        Return " insert into Usuario(usr, pass, rols) values ('" & user._username & "','" & user._password & "'," & user._roles & " ) "
    End Function

    Public Sub insertUsuario(ByRef usr As Usuario)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing

        Try

            cnx = _connectionManager.getConnection()

            command = New OleDbCommand(Me.insertUsuarioQuery(usr), cnx)

            cnx.Open()

            command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

    End Sub

    Private Function updateUsuarioQuery(ByRef user As Usuario)

        Dim foo As String = "update Usuario set usr='" & user._username & "',pass='" & user._password & "', rols=" & user._roles & " where id = " & user._id
        Return foo
    End Function

    Public Function updateUsuario(ByRef user As Usuario)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As Integer = 0
        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.updateUsuarioQuery(user), cnx)

            cnx.Open()

            reader = command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return reader

    End Function

    Private Function deleteUsuarioQuery(ByRef user As Usuario)
        Dim foo As String = "delete from Usuario where id = " & user._id
        Return foo
    End Function

    Public Function deleteUsuario(ByRef user As Usuario)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As Integer = 0
        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.deleteUsuarioQuery(user), cnx)

            cnx.Open()

            reader = command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return reader

    End Function

    Private Function getLastConsecutivoRutaQuery()
        Return "SELECT Max(consecutivo) AS Expr1 FROM CAT_RUTAS"
    End Function

    Public Function getLastConsecutivoRuta()

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim cnt As Long = 0

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getLastConsecutivoRutaQuery(), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                cnt = reader(0)

                If System.DBNull.Value.Equals(reader(0)) Then
                    cnt = 0
                Else
                    cnt = reader(0) + 1
                End If

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return cnt

    End Function


    Private Function getRutasQuery()
        Dim foo As String
        foo = " select id from CAT_RUTAS order by consecutivo asc"
        Return foo
    End Function

    Private Function insertRutaQuery(ByRef ruta As String, ByRef consecutivo As Long)
        Dim foo As String
        foo = " insert into CAT_RUTAS(id, consecutivo) values('" & ruta & "'," & consecutivo & ")"
        Return foo
    End Function

    Private Function deleteRutaQuery(ByRef ruta As String)
        Dim foo As String
        foo = "delete from CAT_RUTAS where id='" & ruta & "'"
        Return foo
    End Function

    Public Function deleteRuta(ByRef ruta As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As Integer = 0
        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.deleteRutaQuery(ruta), cnx)

            cnx.Open()

            reader = command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return reader

    End Function

    Public Function insertRuta(ByRef ruta As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As Integer = 0        

        Dim consecutivo As Long = Me.getLastConsecutivoRuta()

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.insertRutaQuery(ruta, consecutivo), cnx)

            cnx.Open()

            reader = command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return reader

    End Function

    Private Function existeRutaQuery(ByRef ruta As String)
        Dim foo As String = " select count(*) from CAT_RUTAS where id='" & ruta & "'"
        Return foo
    End Function

    Private Function updateRutaQuery(ByRef ruta As String, ByRef newRuta As String)
        Dim foo As String = "update CAT_RUTAS set id='" & newRuta & "' where id = '" & ruta & "'"
        Return foo
    End Function

    Public Function updateRuta(ByRef ruta As String, ByRef newRuta As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As Integer = 0
        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.updateRutaQuery(ruta, newRuta), cnx)

            cnx.Open()

            reader = command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return reader

    End Function

    Public Sub updateRutas(ByRef foo As List(Of String))

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As Integer = 0
        Dim existe = False

        Dim sqlTransaction As OleDbTransaction = Nothing

        Try

            cnx = _connectionManager.getConnection()

            cnx.Open()
            sqlTransaction = cnx.BeginTransaction()
            command = cnx.CreateCommand()
            command.Transaction = sqlTransaction

            command.CommandText = "DELETE FROM CAT_RUTAS"
            command.ExecuteNonQuery()

            Dim k As Long = 0

            For Each ruta In foo
                command.CommandText = "insert into CAT_RUTAS(id,consecutivo) values('" & ruta & "'," & k & ")"
                command.ExecuteNonQuery()
                k = k + 1
            Next

            sqlTransaction.Commit()

        Catch ex As Exception

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Rollback()
            End If

            Throw ex

        Finally

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Dispose()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

    End Sub

    Public Function existRuta(ByRef ruta As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.existeRutaQuery(ruta), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                existe = (reader(0) <> 0)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return existe

    End Function

    Public Function getRutas()

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim rutas As List(Of String) = New List(Of String)

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getRutasQuery(), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                rutas.Add(reader(0))

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return rutas

    End Function

    Private Function getExistRutaViajeNoCerradoQuery(ByRef ruta As String)
        Dim foo As String
        foo = " select count(*) from Carga where ruta='" & ruta & "' and cerrada=0"
        Return foo
    End Function

    Public Function existRutaViajeNoCerrado(ByRef ruta As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getExistRutaViajeNoCerradoQuery(ruta), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                existe = (reader(0) <> 0)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return existe

    End Function

    Private Function updateRutaEnCargaQuery(ByRef ruta As String, ByRef newRuta As String)
        Dim foo As String
        foo = "update Carga set ruta='" & newRuta & "' where ruta='" & ruta & "'"
        Return foo
    End Function

    Private Function deleteRutaEnCargaQuery(ByRef ruta As String)
        Dim foo As String
        foo = "delete from Carga where ruta='" & ruta & "'"
        Return foo
    End Function

    Public Function deleteRutaEnCarga(ByRef ruta As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As Integer = 0
        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.deleteRutaEnCargaQuery(ruta), cnx)

            cnx.Open()

            reader = command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return reader

    End Function

    Public Function updateRutaEnCarga(ByRef ruta As String, ByRef newRuta As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As Integer = 0
        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.updateRutaEnCargaQuery(ruta, newRuta), cnx)

            cnx.Open()

            reader = command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return reader

    End Function

    Private Function getCargaExistQuery(ByRef id As Long)
        Dim foo As String
        foo = " select count(*) from Carga where id=" & id
        Return foo
    End Function

    Public Function existCarga(ByRef id As Long)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getCargaExistQuery(id), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                existe = (reader(0) <> 0)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return existe

    End Function

    Private Function getRegresoExistQuery(ByRef id As Long)
        Dim foo As String
        foo = " select count(*) from Regreso where id=" & id
        Return foo
    End Function

    Public Function existRegreso(ByRef id As Long)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getRegresoExistQuery(id), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                existe = (reader(0) <> 0)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return existe

    End Function

    Private Function getLastCargaQuery(ByRef ruta As String)

        Dim foo As String
        foo = " select id from Carga where ruta='" & ruta & "' and fecha=(select max(fecha) from Carga where ruta='" & ruta & "')"
        Return foo

    End Function

    Public Function getLastCarga(ByRef ruta As String)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim viaje As Long = -1

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getLastCargaQuery(ruta), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                viaje = reader(0)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return viaje

    End Function

    Private Function getPuertaExistQuery(ByRef id As Long)
        Dim foo As String
        foo = " select count(*) from Puerta where id=" & id
        Return foo
    End Function

    Public Function existPuerta(ByRef id As Long)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim existe = False

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getPuertaExistQuery(id), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                existe = (reader(0) <> 0)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return existe

    End Function

    Private Function getCargaQuery(ByRef id As Long)
        Dim foo As String
        foo = " select  ruta,fecha, cerrada, app, apla, hielo, caja355, caja500,caja1500, caja1000,envase from Carga where id=" & id
        Return foo
    End Function

    Public Function getCarga(ByRef id As Long)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim carga As Carga = Nothing

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getCargaQuery(id), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                carga = New Carga()
                carga._id = id
                carga._ruta = reader(0)
                carga._fecha = reader(1)
                carga._cerrada = reader(2)
                carga._app = reader(3)
                carga._apla = reader(4)
                carga._hielo = reader(5)
                carga._caja355 = reader(6)
                carga._caja500 = reader(7)
                carga._caja1500 = reader(8)
                carga._caja1000 = reader(9)
                carga._envase = reader(10)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return carga

    End Function

    Private Function getRegresoQuery(ByRef id As Long)
        Dim foo As String
        foo = " select roto,envase,competencia,lleno, obs,llaves,caja355, caja500, caja1500, prestamo, recuperado, hielo, caja1000 from Regreso where id=" & id
        Return foo
    End Function

    Public Function getRegreso(ByRef id As Long)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim regreso As Regreso = Nothing

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getRegresoQuery(id), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                regreso = New Regreso()
                regreso._id = id
                regreso._roto = reader(0)
                regreso._envase = reader(1)
                regreso._competencia = reader(2)
                regreso._lleno = reader(3)

                If System.DBNull.Value.Equals(reader(4)) Then
                    regreso._obs = String.Empty
                Else
                    regreso._obs = reader(4)
                End If

                regreso._llaves = reader(5)
                regreso._caja355 = reader(6)
                regreso._caja500 = reader(7)
                regreso._caja1500 = reader(8)
                regreso._prestamo = reader(9)
                regreso._recuperado = reader(10)
                regreso._hielo = reader(11)
                regreso._caja1000 = reader(12)
            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return regreso

    End Function

    Private Function getPuertaQuery(ByRef id As Long)
        Dim foo As String
        foo = " select app, apla, hielo, caja355, caja500, caja1500, llaves, caja1000, obs,envase from Puerta where id=" & id
        Return foo
    End Function

    Public Function getPuerta(ByRef id As Long)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim puerta As Puerta = Nothing

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getPuertaQuery(id), cnx)

            cnx.Open()

            reader = command.ExecuteReader()

            While reader.Read()

                puerta = New Puerta()
                puerta._id = id
                puerta._app = reader(0)
                puerta._apla = reader(1)
                puerta._hielo = reader(2)
                puerta._caja355 = reader(3)
                puerta._caja500 = reader(4)
                puerta._caja1500 = reader(5)
                puerta._llaves = reader(6)
                puerta._caja1000 = reader(7)

                If System.DBNull.Value.Equals(reader(8)) Then
                    puerta._obs = String.Empty
                Else
                    puerta._obs = reader(8)
                End If

                puerta._envase = reader(9)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return puerta

    End Function

    Private Function getLastCargaInsert()

        Dim foo As String

        foo = "select max(id) from Carga"

        Return foo

    End Function

    Private Function getCargaInsert(ByRef carga As Carga)
        Dim foo As String
        foo = " insert into Carga (ruta, app, apla, hielo, caja355, caja500,caja1500, caja1000,envase) values ('" & carga._ruta & "'," & carga._app & "," & carga._apla & "," & carga._hielo & "," & carga._caja355 & "," & carga._caja500 & "," & carga._caja1500 & "," & carga._caja1000 & "," & carga._envase & ")"

        Return foo
    End Function

    Public Function cargaInsert(ByRef carga As Carga)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing
        Dim reader As OleDbDataReader = Nothing

        Dim id = -1

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getCargaInsert(carga), cnx)

            cnx.Open()

            command.ExecuteNonQuery()

            command = New OleDbCommand(Me.getLastCargaInsert(), cnx)

            reader = command.ExecuteReader()

            While reader.Read()

                id = reader(0)

            End While

        Catch ex As Exception

            Throw ex

        Finally

            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

        Return id

    End Function

    Private Function getRegresoInsert(ByRef regreso As Regreso)
        Dim foo As String
        foo = " insert into Regreso (id, roto,envase,competencia,lleno,obs,llaves,caja355,caja500,caja1500,prestamo,recuperado,hielo,caja1000) values(" & regreso._id & "," & regreso._roto & "," & regreso._envase & "," & regreso._competencia & "," & regreso._lleno & ",'" & regreso._obs & "'," & regreso._llaves & "," & regreso._caja355 & "," & regreso._caja500 & "," & regreso._caja1500 & "," & regreso._prestamo & "," & regreso._recuperado & "," & regreso._hielo & "," & regreso._caja1000 & ")"
        Return foo
    End Function

    Public Sub regresoInsert(ByRef regreso As Regreso)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getRegresoInsert(regreso), cnx)

            cnx.Open()

            command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

    End Sub

    Private Function getPuertaInsert(ByRef puerta As Puerta)
        Dim foo As String
        foo = " insert into Puerta (id, app, apla, hielo, caja355, caja500,caja1500,llaves, caja1000, obs,envase) values (" & puerta._id & "," & puerta._app & "," & puerta._apla & "," & puerta._hielo & "," & puerta._caja355 & "," & puerta._caja500 & "," & puerta._caja1500 & "," & puerta._llaves & "," & puerta._caja1000 & ",'" & puerta._obs & "'," & puerta._envase & ")"
        Return foo
    End Function

    Public Sub puertaInsert(ByRef puerta As Puerta)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getPuertaInsert(puerta), cnx)

            cnx.Open()

            command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

    End Sub

    Private Function getCargaUpdate(ByRef carga As Carga)

        Dim foo As String

        foo = "update Carga set ruta='" & carga._ruta & "', cerrada=" & carga._cerrada & ",app=" & carga._app & ", apla=" & carga._apla & ", hielo=" & carga._hielo & " , caja355=" & carga._caja355 & ", caja500=" & carga._caja500 & ", caja1500=" & carga._caja1500 & ", caja1000=" & carga._caja1000 & ", envase=" & carga._envase & " where id=" & carga._id

        Return foo

    End Function

    Public Sub cargaUpdate(ByRef carga As Carga)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getCargaUpdate(carga), cnx)

            cnx.Open()

            command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

    End Sub

    Private Function getRegresoUpdate(ByRef regreso As Regreso)

        Dim foo As String

        foo = "update Regreso set roto=" & regreso._roto & ", recuperado=" & regreso._recuperado & ", hielo=" & regreso._hielo _
        & ", prestamo=" & regreso._prestamo & ", llaves=" & regreso._llaves & ", caja355=" & regreso._caja355 & ", caja500=" _
        & regreso._caja500 & ", caja1500=" & regreso._caja1500 & ", caja1000=" & regreso._caja1000 & ", envase=" & regreso._envase _
        & ", competencia=" & regreso._competencia & ", lleno=" & regreso._lleno & ", obs='" & regreso._obs & "' where id=" & regreso._id



        Return foo

    End Function

    Public Sub regresoUpdate(ByRef regreso As Regreso)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getRegresoUpdate(regreso), cnx)

            cnx.Open()

            command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

    End Sub

    Private Function getPuertaUpdate(ByRef puerta As Puerta)

        Dim foo As String

        foo = "update Puerta set app=" & puerta._app & ", apla=" & puerta._apla & ", hielo=" & puerta._hielo & ", llaves=" & puerta._llaves & ", caja355=" & puerta._caja355 & ", caja500=" & puerta._caja500 & ", caja1500=" & puerta._caja1500 & ", caja1000=" & puerta._caja1000 & ", envase=" & puerta._envase & ", obs='" & puerta._obs & "' where id=" & puerta._id

        Return foo

    End Function

    Public Sub puertaUpdate(ByRef puerta As Puerta)

        Dim cnx As OleDbConnection = Nothing
        Dim command As OleDbCommand = Nothing

        Try

            cnx = _connectionManager.getConnection()
            command = New OleDbCommand(Me.getPuertaUpdate(puerta), cnx)

            cnx.Open()

            command.ExecuteNonQuery()

        Catch ex As Exception

            Throw ex

        Finally

            If Not cnx Is Nothing Then
                cnx.Close()
            End If

        End Try

    End Sub

End Class
